


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        cc3
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        core excitation:    {1}
        algorithm:          diis
        singlet states:     2
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.10800
     Total cpu time (sec):               0.17153


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846800     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7380E-10
    10           -78.843851693631     0.1594E-07     0.3553E-12
    11           -78.843851693631     0.3322E-08     0.4263E-13
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.0000E+00
    14           -78.843851693631     0.1280E-09     0.5684E-13
    15           -78.843851693631     0.1479E-10     0.8527E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195128   9   0.704416610872  17   1.682695299885  25   3.175416260672
   2  -1.277667044958  10   0.746260122921  18   1.804186820060  26   3.209661156325
   3  -0.898849406810  11   1.155862024397  19   1.902641648752  27   3.328173215213
   4  -0.629870222684  12   1.170770887084  20   2.148883457177  28   3.721936473482
   5  -0.541641772841  13   1.267961746557  21   2.200395756979  29   3.985492632598
   6  -0.485872762812  14   1.449847537230  22   2.540321860021
   7   0.159756317424  15   1.463234913441  23   2.541517310547
   8   0.229311906285  16   1.474444394533  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.41700
     Total cpu time (sec):               0.70355


  :: CC3 wavefunction
  ======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CC3 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (diis algorithm)


  1) Preparation of MO basis and integrals

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       True


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-09
     Energy threshold:          0.10E-09

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931609     0.9358E-01     0.7908E+02
    2           -79.096425668902     0.2799E-01     0.1218E-01
    3           -79.103829958624     0.6335E-02     0.7404E-02
    4           -79.104336097470     0.1982E-02     0.5061E-03
    5           -79.104390774596     0.5221E-03     0.5468E-04
    6           -79.104398749304     0.1995E-03     0.7975E-05
    7           -79.104389483792     0.4719E-04     0.9266E-05
    8           -79.104389091837     0.1106E-04     0.3920E-06
    9           -79.104388634767     0.4082E-05     0.4571E-06
   10           -79.104388457645     0.1665E-05     0.1771E-06
   11           -79.104388531274     0.6368E-06     0.7363E-07
   12           -79.104388534800     0.2615E-06     0.3526E-08
   13           -79.104388539782     0.9950E-07     0.4982E-08
   14           -79.104388532668     0.3238E-07     0.7114E-08
   15           -79.104388532456     0.8372E-08     0.2118E-09
   16           -79.104388532883     0.2052E-08     0.4269E-09
   17           -79.104388532983     0.6454E-09     0.9983E-10
   18           -79.104388533025     0.1406E-09     0.4209E-10
   19           -79.104388533030     0.4351E-10     0.5272E-11
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.104388533030

     Correlation energy (a.u.):           -0.260536839399

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5       -0.015274304422
       14      4        0.008970023655
        7      4       -0.007312583239
        4      5       -0.006872684665
        2      4       -0.006012921722
       15      5        0.005704150039
        6      2       -0.005025576014
       13      5       -0.004933889033
        5      6        0.004366074625
        3      4       -0.004126391912
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.049931914500
        5      6       5      6       -0.046567925286
        9      3       9      3       -0.041349512985
        3      4       3      4       -0.037778342511
        1      5       1      5       -0.037299297234
        6      5       6      5       -0.034903296770
       16      3      16      3       -0.032107572469
       17      3      17      3       -0.032051843282
        2      4       1      5       -0.031565942815
       18      3      18      3       -0.031349888892
     --------------------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007219544901

  - Finished solving the CC3 ground state equations

     Total wall time (sec):              1.35500
     Total cpu time (sec):               3.86632


  3) Calculation of the excited state (diis algorithm)
     Calculating right vectors

   - DIIS coupled cluster excited state solver
  -----------------------------------------------

  A DIIS solver that solves for the lowest eigenvalues and  the right 
  eigenvectors of the Jacobian matrix, A. The eigenvalue  problem is solved 
  by DIIS extrapolation of residuals for each  eigenvector until the convergence 
  criteria are met.

  More on the DIIS algorithm can be found in P. Pulay, Chemical Physics 
  Letters, 73(2), 393-398 (1980).

  - Settings for coupled cluster excited state solver (DIIS):

     Calculation type:    core
     Excitation vectors:  right

     Energy threshold:                0.10E-09
     Residual threshold:              0.10E-09

     Number of singlet states:               2
     Max number of iterations:             100

     DIIS dimension:                        20

  Iteration:                  1

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     20.324668869402       0.9339E+00
     2     20.387194522383       0.9391E+00
  -----------------------------------------------

  Iteration:                  2

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.750399110734       0.2808E+00
     2     19.811653538880       0.2955E+00
  -----------------------------------------------

  Iteration:                  3

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.686816413840       0.9693E-01
     2     19.754257509075       0.1021E+00
  -----------------------------------------------

  Iteration:                  4

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.694851551008       0.4302E-01
     2     19.757552575918       0.4936E-01
  -----------------------------------------------

  Iteration:                  5

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.694087462900       0.2270E-01
     2     19.755754710388       0.2627E-01
  -----------------------------------------------

  Iteration:                  6

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.694752437187       0.8956E-02
     2     19.756688887799       0.1159E-01
  -----------------------------------------------

  Iteration:                  7

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695851649150       0.3980E-02
     2     19.757837249416       0.5549E-02
  -----------------------------------------------

  Iteration:                  8

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695897485792       0.1949E-02
     2     19.757789188201       0.2848E-02
  -----------------------------------------------

  Iteration:                  9

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695812025561       0.4716E-03
     2     19.757649418717       0.6492E-03
  -----------------------------------------------

  Iteration:                 10

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695818024342       0.1512E-03
     2     19.757642814869       0.2514E-03
  -----------------------------------------------

  Iteration:                 11

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695820657187       0.6181E-04
     2     19.757651656177       0.1520E-03
  -----------------------------------------------

  Iteration:                 12

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814814466       0.3160E-04
     2     19.757646764986       0.1367E-03
  -----------------------------------------------

  Iteration:                 13

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814368745       0.2442E-04
     2     19.757646931365       0.1354E-03
  -----------------------------------------------

  Iteration:                 14

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814524260       0.2218E-04
     2     19.757647829635       0.9327E-04
  -----------------------------------------------

  Iteration:                 15

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695813798862       0.1758E-04
     2     19.757646954926       0.4674E-04
  -----------------------------------------------

  Iteration:                 16

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814440592       0.9292E-05
     2     19.757644028328       0.1776E-04
  -----------------------------------------------

  Iteration:                 17

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814675623       0.3335E-05
     2     19.757643488816       0.6837E-05
  -----------------------------------------------

  Iteration:                 18

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814585994       0.1124E-05
     2     19.757643575791       0.2235E-05
  -----------------------------------------------

  Iteration:                 19

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814494347       0.3855E-06
     2     19.757643734099       0.6746E-06
  -----------------------------------------------

  Iteration:                 20

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814452784       0.1193E-06
     2     19.757643760681       0.2485E-06
  -----------------------------------------------

  Iteration:                 21

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814452645       0.5556E-07
     2     19.757643767052       0.1269E-06
  -----------------------------------------------

  Iteration:                 22

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814453797       0.2882E-07
     2     19.757643771350       0.6372E-07
  -----------------------------------------------

  Iteration:                 23

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814452989       0.7501E-08
     2     19.757643769634       0.2264E-07
  -----------------------------------------------

  Iteration:                 24

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814453354       0.2016E-08
     2     19.757643767796       0.6554E-08
  -----------------------------------------------

  Iteration:                 25

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814453438       0.4616E-09
     2     19.757643767710       0.1848E-08
  -----------------------------------------------

  Iteration:                 26

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814453429       0.1814E-09
     2     19.757643767749       0.6371E-09
  -----------------------------------------------

  Iteration:                 27

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814453425       0.6421E-10
     2     19.757643767739       0.1973E-09
  -----------------------------------------------

  Iteration:                 28

   Root     Eigenvalue (Re)     Residual norm
  -----------------------------------------------
     1     19.695814453425       0.6421E-10
     2     19.757643767736       0.6320E-10
  -----------------------------------------------
  Convergence criterion met in 28 iterations!

  - Resorting roots according to excitation energy.

  - Stored converged states to file.

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                 19.695814453425
     Fraction singles (|R1|/|R|):       0.897028894966

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      1        0.877524880942
        4      1        0.163615316633
        6      1        0.070294513099
       13      1       -0.042616691329
       15      1        0.028545597877
        9      1        0.010920119622
       10      1        0.008789970922
       11      1        0.006652431711
       22      1        0.003588047853
        8      1       -0.003418115405
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        1      1       2      4        0.188307761003
        1      1       5      6       -0.171833630915
        1      1       1      5        0.151788153388
        1      1       6      5        0.149167394372
        1      1       7      4       -0.122673692614
        1      1       1      2       -0.107258664656
        1      1       3      4        0.102703044117
        1      1       4      5        0.091578681422
        1      1      13      2        0.069695813099
        2      1       1      4        0.063050428510
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                 19.757643767736
     Fraction singles (|R1|/|R|):       0.900715877412

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      1        0.866902236611
        3      1        0.206635562157
        7      1       -0.122088605191
       14      1        0.044833689891
       12      1       -0.010994157935
       23      1        0.005598721996
        4      1        0.001207499767
        5      1       -0.000898133918
        1      1       -0.000736344689
       17      1        0.000681888846
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      1       2      4        0.203748977426
        2      1       5      6       -0.165486667822
        2      1       6      5        0.140348351409
        2      1       1      5        0.137483156039
        2      1       7      4       -0.120986364631
        2      1       3      4        0.098697109776
        2      1       4      5        0.090170027452
        2      1       1      2       -0.088369626849
        2      1      13      2        0.067112812003
        2      1      14      4        0.053551666771
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                 19.695814453425      535.950410070448
        2                 19.757643767736      537.632871409521
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CC3 excited state equations (right)

     Total wall time (sec):              3.41800
     Total cpu time (sec):              10.09181

  - Timings for the CC3 excited state calculation

     Total wall time (sec):              4.77800
     Total cpu time (sec):              13.96648

  eT terminated successfully!
