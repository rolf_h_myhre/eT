!
!
!  eT - a coupled cluster program
!  Copyright (C) 2016-2020 the authors of eT
!
!  eT is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  eT is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this program. If not, see <https://www.gnu.org/licenses/>.
!
!
   module subroutine initialize_u_aibj_mlcc2(wf)
!!
!!    Initialize u 
!!    Written by Sarai D. Folkestad and Eirik F. Kjønstad, Jan 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine initialize_u_aibj_mlcc2
!
!
   module subroutine destruct_u_aibj_mlcc2(wf)
!!
!!    Initialize u 
!!    Written by Sarai D. Folkestad and Eirik F. Kjønstad, Jan 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_u_aibj_mlcc2
!
!
   module subroutine initialize_amplitudes_mlcc2(wf)
!!
!!    Initialize amplitudes
!!    Written by Sarai D. Folkestad and Eirik F. Kjønstad, Jan 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine initialize_amplitudes_mlcc2
!
!
   module subroutine destruct_amplitudes_mlcc2(wf)
!!
!!    Destruct amplitudes
!!    Written by Sarai D. Folkestad and Eirik F. Kjønstad, Jan 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_amplitudes_mlcc2
!
!
   module subroutine initialize_x2_mlcc2(wf)
!!
!!    Initialize x2 amplitudes
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine initialize_x2_mlcc2
!
!
   module subroutine initialize_t2bar_mlcc2(wf)
!!
!!    Initialize t2bar amplitudes
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine initialize_t2bar_mlcc2
!
!
   module subroutine destruct_x2_mlcc2(wf)
!!
!!    Destruct x2 amplitudes
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_x2_mlcc2
!
!
   module subroutine destruct_t2bar_mlcc2(wf)
!!
!!    Destruct t2bar amplitudes
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_t2bar_mlcc2
!
!
   module subroutine initialize_nto_states_mlcc2(wf)
!!
!!    Initialize nto states
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine initialize_nto_states_mlcc2
!
!
   module subroutine initialize_cnto_states_mlcc2(wf)
!!
!!    Initialize cnto states
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine initialize_cnto_states_mlcc2
!
!
   module subroutine destruct_nto_states_mlcc2(wf)
!!
!!    Destruct nto states
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_nto_states_mlcc2
!
!
   module subroutine destruct_cnto_states_mlcc2(wf)
!!
!!    Destruct cnto states
!!    Written by Sarai D. Folkestad, 2019
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_cnto_states_mlcc2
!
!
   module subroutine destruct_multipliers_mlcc2(wf)
!!
!!    Destruct multipliers states
!!    Written by Sarai D. Folkestad, Jan 2020
!!
      implicit none
!
      class(mlcc2) :: wf
!
   end subroutine destruct_multipliers_mlcc2
