#!/usr/bin/env python3

# provides os.path.join
import os

# provides exit
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# we import essential functions from the runtest library
from runtest import version_info, get_filter, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# functions to get the filters
from filters import get_hf_filter

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct filter(s)
threshold = 1.0e-10

f = get_hf_filter(threshold)

# QM/PCM energies
f.append(get_filter(from_string='QM/PCM SCF Contribution:',
                    num_lines=2,
                    abs_tolerance=1.0e-10,
                    mask=[1]))

# invoke the command line interface parser which returns options
options = cli()

ierr = 0
for inp in ['hf_energy_pcm_external.inp']:
    # the run function runs the code and filters the outputs
    ierr += run(options,
                configure,
                input_files=inp,
                filters={'out': f},
                extra_args='-pcm water_ief.pcm')

sys.exit(ierr)
