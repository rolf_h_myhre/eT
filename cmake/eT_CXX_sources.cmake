#
#
#   eT - a coupled cluster program
#   Copyright (C) 2016-2020 the authors of eT
#
#   eT is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   eT is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <https://www.gnu.org/licenses/>.
#
#
set(eT_CXX_sources
   src/libint/h_wx.cpp
   src/libint/v_wx.cpp
   src/libint/mu_wx.cpp
   src/libint/q_wx.cpp
   src/libint/g_wxyz.cpp
   src/libint/extract_integrals.cpp
   src/libint/libint_initialization.cpp
   src/libint/atom_init.cpp
   src/libint/s_wx.cpp
)
