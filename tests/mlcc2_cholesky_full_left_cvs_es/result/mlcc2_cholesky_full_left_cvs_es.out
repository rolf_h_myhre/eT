


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
       ground state
       excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     2
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
        left eigenvectors
        core excitation: {1}
     end solver cc es

     active atoms
        selection type: range
        cc2: [1,4]
     end active atoms

     mlcc
        cc2 orbitals: cholesky
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        1 H             cc-pvdz    cc2
        2 H             cc-pvdz    cc2
        3 O             cc-pvdz    cc2
        4 He            cc-pvdz    cc2
     ====================================
     Total number of active atoms: 4
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.10900
     Total cpu time (sec):               0.17106


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7053E-09
     9           -78.843851693630     0.2071E-06     0.7380E-10
    10           -78.843851693631     0.1594E-07     0.3979E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.8527E-13
    13           -78.843851693631     0.4264E-09     0.4263E-13
    14           -78.843851693631     0.1280E-09     0.5684E-13
    15           -78.843851693631     0.1479E-10     0.4263E-13
    16           -78.843851693631     0.2906E-11     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195145   9   0.704416610867  17   1.682695299877  25   3.175416260660
   2  -1.277667044968  10   0.746260122914  18   1.804186820052  26   3.209661156313
   3  -0.898849406810  11   1.155862024383  19   1.902641648742  27   3.328173215200
   4  -0.629870222690  12   1.170770887076  20   2.148883457168  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756969  29   3.985492632586
   6  -0.485872762832  14   1.449847537220  22   2.540321860021
   7   0.159756317416  15   1.463234913438  23   2.541517310547
   8   0.229311906283  16   1.474444394527  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.45700
     Total cpu time (sec):               0.77915


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.5551E-16

     The smallest diagonal after decomposition is:  -0.8882E-15

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - MLCC2 orbital partitioning:

     Orbital type: cholesky

     Number occupied cc2 orbitals:    6
     Number virtual cc2 orbitals:    23

     Number occupied ccs orbitals:    0
     Number virtual ccs orbitals:     0

  Warning: no ccs orbitals in mlcc2 calculation, recomended to run standard 
           cc2 code.


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-10
     Energy threshold:          0.10E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.3734E-01     0.7908E+02
    2           -79.085431671929     0.7567E-02     0.1190E-02
    3           -79.085759807021     0.9940E-03     0.3281E-03
    4           -79.085768585650     0.1833E-03     0.8779E-05
    5           -79.085769937826     0.3615E-04     0.1352E-05
    6           -79.085769725742     0.6004E-05     0.2121E-06
    7           -79.085769729164     0.2063E-05     0.3422E-08
    8           -79.085769729229     0.3646E-06     0.6527E-10
    9           -79.085769729230     0.3393E-07     0.9095E-12
   10           -79.085769729553     0.4633E-08     0.3225E-09
   11           -79.085769729585     0.9303E-09     0.3266E-10
   12           -79.085769729575     0.2416E-09     0.1073E-10
   13           -79.085769729575     0.4926E-10     0.4547E-12
   14           -79.085769729575     0.6369E-11     0.3837E-12
  ---------------------------------------------------------------
  Convergence criterion met in 14 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.085769729575

     Correlation energy (a.u.):           -0.241918035945

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5       -0.015071367767
       14      4       -0.009516336778
        7      4       -0.008547796583
       15      5       -0.006232215147
        5      6       -0.005875107063
        6      2        0.005220429230
       13      5       -0.005212699581
        2      4        0.005071084622
       11      6       -0.003616248565
        4      5        0.003233309228
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007125912546

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.11800
     Total cpu time (sec):               0.22562


  3) Calculation of the excited state (davidson algorithm)
     Calculating left vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    core
     Excitation vectors:  left

     Energy threshold:                0.10E-10
     Residual threshold:              0.10E-10

     Number of singlet states:               2
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  20.320582403886    0.000000000000     0.2007E+01   0.2032E+02
     2  20.384108490490    0.000000000000     0.1988E+01   0.2038E+02
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.730105806915    0.000000000000     0.2405E+00   0.5905E+00
     2  19.798192484936    0.000000000000     0.2706E+00   0.5859E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.725837565103    0.000000000000     0.3243E-01   0.4268E-02
     2  19.793110037323    0.000000000000     0.4031E-01   0.5082E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.725247091673    0.000000000000     0.2607E-01   0.5905E-03
     2  19.791966451955    0.000000000000     0.3958E-01   0.1144E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.724064204779    0.000000000000     0.9478E-02   0.1183E-02
     2  19.789399892217    0.000000000000     0.1096E-01   0.2567E-02
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723951341025    0.000000000000     0.2419E-02   0.1129E-03
     2  19.789276712030    0.000000000000     0.5451E-02   0.1232E-03
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936109533    0.000000000000     0.6852E-03   0.1523E-04
     2  19.789297456566    0.000000000000     0.1591E-02   0.2074E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723932972358    0.000000000000     0.1600E-03   0.3137E-05
     2  19.789313278600    0.000000000000     0.4121E-03   0.1582E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723935741995    0.000000000000     0.5847E-04   0.2770E-05
     2  19.789326384222    0.000000000000     0.3935E-04   0.1311E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936309730    0.000000000000     0.9625E-05   0.5677E-06
     2  19.789326476224    0.000000000000     0.1056E-04   0.9200E-07
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936399773    0.000000000000     0.3597E-05   0.9004E-07
     2  19.789326463674    0.000000000000     0.1640E-05   0.1255E-07
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936404237    0.000000000000     0.6181E-06   0.4464E-08
     2  19.789326448956    0.000000000000     0.3137E-06   0.1472E-07
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409650    0.000000000000     0.1601E-06   0.5413E-08
     2  19.789326448755    0.000000000000     0.4683E-07   0.2010E-09
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409856    0.000000000000     0.2317E-07   0.2059E-09
     2  19.789326448880    0.000000000000     0.1185E-07   0.1254E-09
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409633    0.000000000000     0.5539E-08   0.2226E-09
     2  19.789326448873    0.000000000000     0.3529E-08   0.7070E-11
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409607    0.000000000000     0.5284E-09   0.2611E-10
     2  19.789326448855    0.000000000000     0.1309E-08   0.1798E-10
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   34

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409602    0.000000000000     0.1896E-09   0.5748E-11
     2  19.789326448863    0.000000000000     0.1503E-09   0.7738E-11
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409603    0.000000000000     0.4395E-10   0.1425E-11
     2  19.789326448864    0.000000000000     0.2118E-10   0.8242E-12
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   38

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1  19.723936409603    0.000000000000     0.8482E-11   0.4619E-12
     2  19.789326448864    0.000000000000     0.4838E-11   0.2487E-13
  ------------------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                 19.723936409603
     Fraction singles (|L1|/|L|):       0.833588393695

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      1        0.823294735130
        4      1       -0.111712314462
        6      1       -0.051260776398
       13      1       -0.035778191198
       15      1       -0.022771196440
        9      1       -0.008395739156
       10      1       -0.006087185384
       11      1       -0.005313512766
        8      1        0.002597172521
       22      1        0.002218116789
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                 19.789326448864
     Fraction singles (|L1|/|L|):       0.844520289957

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        2      1       -0.825528980417
        3      1       -0.145830361812
        7      1       -0.094677496515
       14      1       -0.037232072920
       12      1        0.009103177812
       23      1        0.003865291543
        4      1       -0.000887746756
        5      1       -0.000684364819
       17      1        0.000650789664
        1      1       -0.000510479029
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                 19.723936409603      536.715647475651
        2                 19.789326448864      538.495001075823
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (left)

     Total wall time (sec):              0.31900
     Total cpu time (sec):               0.57543

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.45900
     Total cpu time (sec):               0.83727

  :: There was 1 warning during the execution of eT. ::

  eT terminated successfully!
