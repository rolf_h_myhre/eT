


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        omega threshold:  1.0d-12
        energy threshold: 1.0d-12
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-12
        energy threshold:   1.0d-12
        right eigenvectors
     end solver cc es

     mlcc
        cc2 orbitals: cnto-approx
        cnto occupied cc2: 2
        cnto states: {1,2,3,4}
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-11
     Gradient threshold:            0.1000E-11

     Coulomb screening threshold:   0.1000E-17
     Exchange screening threshold:  0.1000E-15
     Fock precision:                0.1000E-35
     Integral cutoff:               0.1000E-17

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836315
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592587     0.9053E-01     0.7880E+02
     2           -78.828675852655     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693556     0.2356E-05     0.7053E-09
     9           -78.843851693630     0.2071E-06     0.7388E-10
    10           -78.843851693631     0.1594E-07     0.2842E-12
    11           -78.843851693631     0.3322E-08     0.1421E-13
    12           -78.843851693631     0.1197E-08     0.8527E-13
    13           -78.843851693631     0.4264E-09     0.2842E-13
    14           -78.843851693631     0.1280E-09     0.1421E-13
    15           -78.843851693631     0.1479E-10     0.0000E+00
    16           -78.843851693631     0.2905E-11     0.0000E+00
    17           -78.843851693631     0.6422E-12     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080251
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195142   9   0.704416610867  17   1.682695299878  25   3.175416260660
   2  -1.277667044967  10   0.746260122915  18   1.804186820053  26   3.209661156313
   3  -0.898849406811  11   1.155862024381  19   1.902641648743  27   3.328173215200
   4  -0.629870222687  12   1.170770887076  20   2.148883457169  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756970  29   3.985492632586
   6  -0.485872762834  14   1.449847537220  22   2.540321860021
   7   0.159756317417  15   1.463234913437  23   2.541517310547
   8   0.229311906285  16   1.474444394527  24   2.559338707349
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.78500
     Total cpu time (sec):               1.41832


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.32800
     Total cpu time (sec):               0.55639


  2) Preparation of MO basis and integrals

  Running CCS calculation for NTOs/CNTOs.

  - Summary of CCS calculation for NTOs/CNTOs:

     Wall time for CCS ground calculation (sec):                   0.02
     CPU time for CCS ground calculation (sec):                    0.03

     Wall time for CCS excited calculation (sec):                  0.08
     CPU time for CCS excited calculation (sec):                   0.16

  - MLCC2 orbital partitioning:

     Orbital type: cnto-approx

     Number occupied cc2 orbitals:    2
     Number virtual cc2 orbitals:     6

     Number occupied ccs orbitals:    4
     Number virtual ccs orbitals:    17


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-11
     Energy threshold:          0.10E-11

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -78.880002672016     0.1416E-01     0.7888E+02
    2           -78.880231596386     0.3907E-02     0.2289E-03
    3           -78.880312811809     0.1105E-02     0.8122E-04
    4           -78.880333396809     0.2255E-03     0.2059E-04
    5           -78.880333774021     0.5012E-04     0.3772E-06
    6           -78.880333764595     0.1015E-04     0.9426E-08
    7           -78.880333763375     0.1859E-05     0.1220E-08
    8           -78.880333762738     0.4004E-06     0.6367E-09
    9           -78.880333762134     0.1349E-06     0.6045E-09
   10           -78.880333762004     0.4092E-07     0.1302E-09
   11           -78.880333762107     0.6703E-08     0.1037E-09
   12           -78.880333762127     0.1018E-08     0.1994E-10
   13           -78.880333762125     0.1487E-09     0.2686E-11
   14           -78.880333762123     0.3466E-10     0.1336E-11
   15           -78.880333762123     0.8351E-11     0.1705E-12
   16           -78.880333762123     0.2128E-11     0.2842E-13
   17           -78.880333762123     0.5747E-12     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -78.880333762123

     Correlation energy (a.u.):           -0.036482068492

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      1        0.013830150527
       12      1        0.003417055288
       15      1       -0.003111637605
        5      2       -0.002917487323
        3      1       -0.002220949710
       11      2       -0.002083639763
        1      4        0.002063497774
       10      1       -0.001871388233
        6      1       -0.001853304679
        3      4       -0.001850346727
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.004603877530

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.14100
     Total cpu time (sec):               0.28213


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-11
     Residual threshold:              0.10E-11

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.321605807423    0.000000000000     0.2763E+00   0.3216E+00
     2   0.387427580910    0.000000000000     0.2954E+00   0.3874E+00
     3   0.431113922735    0.000000000000     0.2716E+00   0.4311E+00
     4   0.475067402437    0.000000000000     0.2699E+00   0.4751E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.268573248868    0.000000000000     0.3428E-01   0.5303E-01
     2   0.323673551052    0.000000000000     0.3884E-01   0.6375E-01
     3   0.373702657561    0.000000000000     0.4915E-01   0.5741E-01
     4   0.424717754175    0.000000000000     0.3308E-01   0.5035E-01
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267739214816    0.000000000000     0.1264E-01   0.8340E-03
     2   0.321939138312    0.000000000000     0.1438E-01   0.1734E-02
     3   0.370921503999    0.000000000000     0.2372E-01   0.2781E-02
     4   0.423036859292    0.000000000000     0.2079E-01   0.1681E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267810811989    0.000000000000     0.2930E-02   0.7160E-04
     2   0.321934878888    0.000000000000     0.2058E-02   0.4259E-05
     3   0.371448986443    0.000000000000     0.5685E-02   0.5275E-03
     4   0.422724562868    0.000000000000     0.1449E-01   0.3123E-03
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267791777406    0.000000000000     0.5159E-03   0.1903E-04
     2   0.321904775871    0.000000000000     0.2832E-03   0.3010E-04
     3   0.371336331405    0.000000000000     0.1504E-02   0.1127E-03
     4   0.422067883879    0.000000000000     0.1029E-01   0.6567E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792960665    0.000000000000     0.7674E-04   0.1183E-05
     2   0.321906517326    0.000000000000     0.1650E-04   0.1741E-05
     3   0.371330359915    0.000000000000     0.3134E-03   0.5971E-05
     4   0.421940912446    0.000000000000     0.2830E-02   0.1270E-03
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792505329    0.000000000000     0.7752E-05   0.4553E-06
     2   0.321906397998    0.000000000000     0.3559E-05   0.1193E-06
     3   0.371334007194    0.000000000000     0.6967E-04   0.3647E-05
     4   0.421959951369    0.000000000000     0.7293E-03   0.1904E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792493685    0.000000000000     0.1428E-05   0.1164E-07
     2   0.321906403931    0.000000000000     0.7125E-06   0.5933E-08
     3   0.371333085518    0.000000000000     0.1885E-04   0.9217E-06
     4   0.421950067790    0.000000000000     0.1913E-03   0.9884E-05
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792493896    0.000000000000     0.2985E-06   0.2116E-09
     2   0.321906407782    0.000000000000     0.1374E-06   0.3850E-08
     3   0.371333234263    0.000000000000     0.3426E-05   0.1487E-06
     4   0.421950583119    0.000000000000     0.3753E-04   0.5153E-06
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494240    0.000000000000     0.5493E-07   0.3438E-09
     2   0.321906407145    0.000000000000     0.2055E-07   0.6370E-09
     3   0.371333275764    0.000000000000     0.6503E-06   0.4150E-07
     4   0.421950710181    0.000000000000     0.6347E-05   0.1271E-06
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494328    0.000000000000     0.1066E-07   0.8834E-10
     2   0.321906407202    0.000000000000     0.4770E-08   0.5707E-10
     3   0.371333270074    0.000000000000     0.1727E-06   0.5690E-08
     4   0.421950675920    0.000000000000     0.1051E-05   0.3426E-07
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494309    0.000000000000     0.1314E-08   0.1921E-10
     2   0.321906407184    0.000000000000     0.6340E-09   0.1783E-10
     3   0.371333270736    0.000000000000     0.2252E-07   0.6613E-09
     4   0.421950679489    0.000000000000     0.1595E-06   0.3569E-08
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494311    0.000000000000     0.1342E-09   0.1540E-11
     2   0.321906407185    0.000000000000     0.6136E-10   0.1452E-11
     3   0.371333270700    0.000000000000     0.2918E-08   0.3515E-10
     4   0.421950679152    0.000000000000     0.2002E-07   0.3365E-09
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494311    0.000000000000     0.1812E-10   0.6384E-14
     2   0.321906407185    0.000000000000     0.7747E-11   0.4935E-13
     3   0.371333270706    0.000000000000     0.3657E-09   0.5367E-11
     4   0.421950679115    0.000000000000     0.2993E-08   0.3734E-10
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494311    0.000000000000     0.1514E-11   0.1787E-13
     2   0.321906407185    0.000000000000     0.1056E-11   0.2059E-13
     3   0.371333270705    0.000000000000     0.4934E-10   0.5240E-12
     4   0.421950679115    0.000000000000     0.2462E-09   0.4855E-12
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494311    0.000000000000     0.1170E-12   0.3997E-14
     2   0.321906407185    0.000000000000     0.8894E-13   0.1776E-14
     3   0.371333270705    0.000000000000     0.5498E-11   0.4557E-13
     4   0.421950679115    0.000000000000     0.4023E-10   0.6083E-12
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   66

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494311    0.000000000000     0.1111E-12   0.5829E-14
     2   0.321906407185    0.000000000000     0.7929E-13   0.1832E-14
     3   0.371333270705    0.000000000000     0.5495E-12   0.2909E-13
     4   0.421950679115    0.000000000000     0.8137E-11   0.1437E-12
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   67

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.267792494311    0.000000000000     0.1109E-12   0.1721E-14
     2   0.321906407185    0.000000000000     0.7678E-13   0.2887E-14
     3   0.371333270705    0.000000000000     0.5323E-12   0.2554E-14
     4   0.421950679115    0.000000000000     0.5327E-12   0.1715E-13
  ------------------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.267792494311
     Fraction singles (|R1|/|R|):       0.991921199283

     MLCC diagnostics:

     |R1^internal|/|R| =       0.991734118163
     |R1^internal|/|R1| =      0.999811395180

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      2       -0.983890878773
        3      2        0.123956334454
       19      1       -0.008959196147
       12      2        0.008181945870
        1      1        0.008107291048
        9      6        0.007740921330
        6      2       -0.007370409072
       20      6        0.007112262385
       11      1        0.006081024707
       21      2       -0.003686886295
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.321906407185
     Fraction singles (|R1|/|R|):       0.992729226266

     MLCC diagnostics:

     |R1^internal|/|R| =       0.992650942929
     |R1^internal|/|R1| =      0.999921143314

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      2       -0.976773677550
        4      2        0.176756413902
       14      2        0.008010245825
        9      4       -0.005616007128
        2      1        0.004841403070
       20      4        0.003605171723
       11      6        0.003487526545
       23      2       -0.003330340231
        5      6       -0.002811662303
        4      5       -0.002113597040
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.371333270705
     Fraction singles (|R1|/|R|):       0.993361277436

     MLCC diagnostics:

     |R1^internal|/|R| =       0.973121629671
     |R1^internal|/|R1| =      0.979625088853

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      1        0.968785461099
        2      6       -0.182808856931
        3      1       -0.074078954553
        4      6        0.069053437542
        5      2        0.045607145387
        1      4        0.028238806186
        6      1       -0.028163439706
        3      4       -0.015250591756
        6      4       -0.013677966522
       15      1        0.008433724710
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.421950679115
     Fraction singles (|R1|/|R|):       0.991880244184

     MLCC diagnostics:

     |R1^internal|/|R| =       0.968589417945
     |R1^internal|/|R1| =      0.976518509794

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      1        0.960957970009
        1      6       -0.198963603051
        4      1       -0.121261515746
        3      6        0.064295380158
        7      1       -0.021066559426
        6      6        0.020439814650
        9      2        0.017346611974
        2      4       -0.013703552364
       10      6       -0.013403819561
       20      2        0.010295389720
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.267792494311        7.287004935949
        2                  0.321906407185        8.759519508230
        3                  0.371333270705       10.104492971227
        4                  0.421950679115       11.481862810795
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (right)

     Total wall time (sec):              0.18600
     Total cpu time (sec):               0.32237

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.78500
     Total cpu time (sec):               1.42000

  eT terminated successfully!
