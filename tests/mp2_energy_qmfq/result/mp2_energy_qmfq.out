


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O-H2O-FQ
        charge: 0
     end system

     do
       ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     solver cholesky
        threshold: 1.0d-11
     end solver cholesky

     molecular mechanics
        forcefield: fq
     end molecular mechanics

     method
        hf
        mp2
     end method


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications (QM)
  ==========================================

     Name:             h2o-h2o-fq
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         24
     Cartesian basis functions:    25
     Primitive basis functions:    49

     Nuclear repulsion energy (a.u.):              9.307879526626
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.009319000000     1.133156000000     0.000000000000        1
        2 H      0.023452000000     0.185621000000     0.000000000000        2
        3 H      0.906315000000     1.422088000000     0.000000000000        3
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.017610357755     2.141354496408     0.000000000000        1
        2 H      0.044317857073     0.350772852968     0.000000000000        2
        3 H      1.712687132585     2.687356845030     0.000000000000        3
     ==============================================================================


  :: Molecular system specifications (MM)
  ==========================================

     Force Field:  fq
     Algorithm  :  mat_inversion

     Number of MM atoms:                   3
     Number of MM molecules:               1

    ====================================================================
                        MM Geometry (Å) and Parameters
    ====================================================================
    Atom    Mol         X          Y          Z         Chi        Eta
    ====================================================================
      O       1    -0.042964  -1.404707  -0.000000   0.685392   0.366624
      H       1    -0.419020  -1.818953   0.760190   0.499339   0.824306
      H       1    -0.419020  -1.818953  -0.760190   0.499339   0.824306
    ====================================================================


  :: RHF wavefunction
  ======================

  This is a QM/MM calculation

     Polarizable Embedding: Fluctuating Charges (FQ) Force Field

     Each atom of the MM portion is endowed with a charge which value 
     can vary in agreement with the ElectronegativityEqualization Principle 
     (EEP), which states that at the equilibrium each atom has the same 
     electronegativity.

     The force field is defined in terms of electronegativity (Chi) and 
     chemical hardness (Eta), which are specified for each MM atom.

     The QM/MM electrostatic interaction energy is defined as:

        E^ele_QM/MM = sum_i q_i * V_i(P)

     where V_i(P) is the electrostatic potential due to the QMdensity 
     calculated at the position of the i-th charge q_i.The values of the 
     charges are obtained by solving a linearequation:

        Dq = -Chi - V(P)

     For further details, see:
     C. Cappelli. IJQC, 2016, 116, 1532-1542.

     CC calculation: zero-order approximation
     FQ charges only affect MOs and Fock


  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:         19
     Number of molecular orbitals:       24
     Number of atomic orbitals:          24


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -76.062235942153
     Number of electrons in guess:           10.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -76.167279756518     0.8765E-01     0.7617E+02
     2           -76.197738767140     0.5225E-01     0.3046E-01
     3           -76.206846897171     0.5938E-02     0.9108E-02
     4           -76.207017689704     0.1052E-02     0.1708E-03
     5           -76.207030611240     0.1541E-03     0.1292E-04
     6           -76.207030952283     0.3075E-04     0.3410E-06
     7           -76.207030963599     0.2102E-05     0.1132E-07
     8           -76.207030963669     0.6179E-06     0.7013E-10
     9           -76.207030963674     0.1630E-06     0.4789E-11
    10           -76.207030963674     0.3461E-07     0.4263E-12
    11           -76.207030963674     0.4998E-08     0.4263E-13
    12           -76.207030963674     0.1125E-08     0.7105E-13
    13           -76.207030963674     0.2481E-09     0.9948E-13
    14           -76.207030963674     0.8065E-10     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 14 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.678594129412
     Nuclear repulsion energy:       9.307879526626
     Electronic energy:            -85.514910490300
     Total energy:                 -76.207030963674

  - Summary of QM/MM energetics:
                                         a.u.             eV     kcal/mol
     QM/MM SCF Contribution:        -0.185843425379
     QM/MM Electrostatic Energy:    -0.047894522916    -1.30328   -30.054

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.467446747437   7   0.408029032492  13   1.545907756442  19   2.647770951988
   2  -1.263412577108   8   0.885760158996  14   1.558611930852  20   3.358494521730
   3  -0.630396552895   9   0.963764657476  15   1.780478947573  21   3.437577336196
   4  -0.491021893205  10   1.231125361121  16   1.947044486631  22   3.590447188220
   5  -0.420458911599  11   1.267871248677  17   2.028727537771  23   3.984049679300
   6   0.258135217813  12   1.327920236221  18   2.574580643208  24   4.227444004111
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.36372
     Total cpu time (sec):               0.72277


  :: MP2 wavefunction
  ======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    5
     Virtual orbitals:     19
     Molecular orbitals:   24
     Atomic orbitals:      24

   - Number of ground state amplitudes:

     Single excitation amplitudes:  95


  :: Ground state coupled cluster engine
  =========================================

  Calculates the ground state CC wavefunction | CC > = exp(T) | R >

  This is a MP2 ground state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-10
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    24
     Total number of shell pairs:            78
     Total number of AO pairs:              300

     Significant shell pairs:                78
     Significant AO pairs:                  300

     Construct shell pairs:                  78
     Construct AO pairs:                    300

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               285 /      71       0.47383E+01         150             29              8265
     2               220 /      60       0.46828E-01         242             99             21780
     3               141 /      47       0.46528E-03         158            157             22137
     4               105 /      33       0.45055E-05         122            226             23730
     5                12 /       8       0.45032E-07          55            271              3252
     6                 0 /       0       0.23634E-09           9            279                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 279

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.1995E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.12682
     Total cpu time (sec):               0.25482


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

  :: Summary of MP2 wavefunction energetics (a.u.)

     HF energy:                   -76.207030963674
     MP2 correction:               -0.201403215155
     MP2 energy:                  -76.408434178830

  - Timings for the MP2 ground state calculation

     Total wall time (sec):              0.12718
     Total cpu time (sec):               0.25518

     Peak memory usage during the execution of eT: 5.864544 MB

  eT terminated successfully!
