


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        restart
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        restart
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        restart
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es

     active atoms
        selection type: list
        cc2: {3}
     end active atoms

     mlcc
        cc2 orbitals: cholesky
        cholesky threshold: 1.0d-1
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        3 O             cc-pvdz    cc2
     ====================================
     Total number of active atoms: 1
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.075790000000     5.000000000000        3
        2 H      0.866810000000     0.601440000000     5.000000000000        1
        3 H     -0.866810000000     0.601440000000     5.000000000000        2
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.143222342981     9.448630622825        3
        2 H      1.638033502034     1.136556880358     9.448630622825        1
        3 H     -1.638033502034     1.136556880358     9.448630622825        2
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Calculation of reference state (SCF-DIIS algorithm)


  1) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-11
     Gradient threshold:            0.1000E-11

     Coulomb screening threshold:   0.1000E-17
     Exchange screening threshold:  0.1000E-15
     Fock precision:                0.1000E-35
     Integral cutoff:               0.1000E-17

  - Requested restart. Reading orbitals from file

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.843851693631     0.6421E-12     0.7884E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  Note: the gradient converged in the first iteration,  so the energy 
  convergence has not been tested!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080251
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195142   9   0.704416610867  17   1.682695299878  25   3.175416260660
   2  -1.277667044967  10   0.746260122915  18   1.804186820053  26   3.209661156313
   3  -0.898849406811  11   1.155862024381  19   1.902641648743  27   3.328173215200
   4  -0.629870222687  12   1.170770887076  20   2.148883457169  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756970  29   3.985492632586
   6  -0.485872762834  14   1.449847537220  22   2.540321860021
   7   0.159756317417  15   1.463234913437  23   2.541517310547
   8   0.229311906285  16   1.474444394527  24   2.559338707349
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.01900
     Total cpu time (sec):               0.03689


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.35200
     Total cpu time (sec):               0.57600


  2) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.3481E-16

     The smallest diagonal after decomposition is:  -0.1892E-13

  - MLCC2 orbital partitioning:

     Orbital type: cholesky

     Number occupied cc2 orbitals:    5
     Number virtual cc2 orbitals:    13

     Number occupied ccs orbitals:    1
     Number virtual ccs orbitals:    10


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-09
     Energy threshold:          0.10E-09

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Requested restart. Reading in solution from file.

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -78.992658321885     0.8213E-12     0.7899E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  Note: the omega vector converged in the first iteration,  so the energy 
  convergence has not been tested!

  - Ground state summary:

     Final ground state energy (a.u.):   -78.992658321885

     Correlation energy (a.u.):           -0.148806628254

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
       15      4        0.016328878160
        5      3       -0.014564153928
        6      4       -0.012826074950
       17      3        0.012491270947
        3      3        0.009204805602
       14      3       -0.007937198480
        2      5       -0.006832883845
       20      3        0.003941989393
        1      2       -0.003004112650
       15      2        0.002970409243
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.009412263873

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.01100
     Total cpu time (sec):               0.01945


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-09
     Residual threshold:              0.10E-09

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Requested restart - restarting 4 right eigenvectors from file.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.2152E-12   0.3114E+00
     2   0.364807620015    0.000000000000     0.1168E-12   0.3648E+00
     3   0.405815979832    0.000000000000     0.3262E-12   0.4058E+00
     4   0.457308246773    0.000000000000     0.3481E-12   0.4573E+00
  ------------------------------------------------------------------------
  Note: Residual(s) converged in first iteration. Energy convergence therefore 
  not tested in this calculation.
  Convergence criterion met in 1 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.311438820474
     Fraction singles (|R1|/|R|):       0.998500898646

     MLCC diagnostics:

     |R1^internal|/|R| =       0.556319394931
     |R1^internal|/|R1| =      0.557154626185

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       15      5       -0.812868167351
        6      5        0.516170330335
        4      5        0.180237956222
       16      5        0.162234469704
       12      5        0.065368798023
        1      5        0.054735307894
        7      5       -0.043283525692
        8      5       -0.024658717929
        2      5        0.019694008118
       11      5       -0.014132968046
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.364807620015
     Fraction singles (|R1|/|R|):       0.997950547483

     MLCC diagnostics:

     |R1^internal|/|R| =       0.660097599964
     |R1^internal|/|R1| =      0.661453216924

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       17      5       -0.547208412300
        3      5       -0.523393275766
       14      5        0.503802434509
        5      5        0.400124089716
       20      5       -0.081570718826
       13      5       -0.039917247008
       14      6        0.008440416703
        3      6       -0.006838474397
       17      6       -0.006543512101
        2      5       -0.006371125459
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.405815979832
     Fraction singles (|R1|/|R|):       0.998318120755

     MLCC diagnostics:

     |R1^internal|/|R| =       0.561879932187
     |R1^internal|/|R1| =      0.562826538461

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       15      4       -0.798832610849
        6      4        0.499364807378
        4      4        0.197740154271
       16      4        0.148449375703
       17      3        0.117055922694
        3      3        0.099743539335
        5      3       -0.091722996917
       14      3       -0.077135260119
       12      4        0.052835634105
        2      5        0.045798272361
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.457308246773
     Fraction singles (|R1|/|R|):       0.998200691097

     MLCC diagnostics:

     |R1^internal|/|R| =       0.627728553611
     |R1^internal|/|R1| =      0.628860067128

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       14      4       -0.524937949090
       17      4        0.496334167672
        3      4        0.476062084508
        5      4       -0.355598561799
       15      3       -0.259240725972
        6      3        0.180075291595
       16      3        0.083997747618
       20      4        0.075844096163
        4      3        0.065668145124
       13      4        0.047168737712
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.311438820474        8.474681965522
        2                  0.364807620015        9.926920971276
        3                  0.405815979832       11.042815280303
        4                  0.457308246773       12.443991233063
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (right)

     Total wall time (sec):              0.03900
     Total cpu time (sec):               0.06614

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.41800
     Total cpu time (sec):               0.69361

  eT terminated successfully!
