


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
     end method

     hf mean value
        dipole
     end hf mean value


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             hof he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         38
     Cartesian basis functions:    40
     Primitive basis functions:    84

     Nuclear repulsion energy (a.u.):             48.518317619727
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 F     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 F     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         28
     Number of molecular orbitals:       38
     Number of atomic orbitals:          38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)
     3) Calculate dipole and/or quadrupole moments


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:              -178.316362504350
     Number of electrons in guess:           20.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.452442672055     0.7565E-01     0.1775E+03
     2          -177.479038695766     0.1346E-01     0.2660E-01
     3          -177.480034970238     0.4311E-02     0.9963E-03
     4          -177.480161392961     0.1845E-02     0.1264E-03
     5          -177.480175069916     0.3629E-03     0.1368E-04
     6          -177.480176290418     0.7402E-04     0.1221E-05
     7          -177.480176366127     0.2207E-04     0.7571E-07
     8          -177.480176376357     0.5096E-05     0.1023E-07
     9          -177.480176377182     0.1218E-05     0.8252E-09
    10          -177.480176377209     0.2536E-06     0.2663E-10
    11          -177.480176377210     0.4399E-07     0.1080E-11
    12          -177.480176377210     0.8545E-08     0.2558E-12
    13          -177.480176377210     0.2855E-08     0.1990E-12
    14          -177.480176377210     0.1321E-08     0.2842E-13
    15          -177.480176377210     0.3976E-09     0.0000E+00
    16          -177.480176377210     0.1465E-09     0.5684E-13
    17          -177.480176377210     0.5394E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.600136348419
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -225.998493996937
     Total energy:                -177.480176377210

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -26.334215397245  11   0.160499885373  21   1.561381025906  31   3.061115047186
   2 -20.644676143358  12   0.548524783449  22   1.686298886871  32   3.534199119712
   3  -1.894514195239  13   0.716123096566  23   1.771732504117  33   3.762918741762
   4  -1.204323614961  14   1.106561921991  24   2.108007790831  34   3.962288761022
   5  -0.910581317589  15   1.130678945475  25   2.501253825861  35   4.000546434602
   6  -0.866359982089  16   1.195125644610  26   2.530943514170  36   4.553973954989
   7  -0.832531025270  17   1.332305214042  27   2.531186114993  37   4.580623650081
   8  -0.790125272749  18   1.423088812982  28   2.601426569333  38   5.083709277094
   9  -0.497921814661  19   1.453320735170  29   2.725832254841
  10  -0.439636463046  20   1.486906665119  30   2.888293377057
  -----------------------------------------------------------------------------------


  3) Calculate dipole and/or quadrupole moments

  - Operator: dipole moment [a.u.]

     x:          0.5754728
     y:          0.4435941
     z:          0.0031142

     |mu|:       0.7266047

  - Operator: dipole moment [Debye]

     x:          1.4627060
     y:          1.1275036
     z:          0.0079156

     |mu|:       1.8468449

  - Timings for the RHF ground state calculation

     Total wall time (sec):              1.07900
     Total cpu time (sec):               1.95410

  eT terminated successfully!
