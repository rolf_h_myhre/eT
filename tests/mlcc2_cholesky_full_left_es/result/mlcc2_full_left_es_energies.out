


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        left eigenvectors
     end solver cc es

     active atoms
        selection type: range
        cc2: [1,4]
     end active atoms

     mlcc
        cc2 orbitals: cholesky
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        1 H             cc-pvdz    cc2
        2 H             cc-pvdz    cc2
        3 O             cc-pvdz    cc2
        4 He            cc-pvdz    cc2
     ====================================
     Total number of active atoms: 4
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.10700
     Total cpu time (sec):               0.16822


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7381E-10
    10           -78.843851693631     0.1594E-07     0.4121E-12
    11           -78.843851693631     0.3322E-08     0.4263E-13
    12           -78.843851693631     0.1197E-08     0.2842E-13
    13           -78.843851693631     0.4264E-09     0.5684E-13
    14           -78.843851693631     0.1280E-09     0.4263E-13
    15           -78.843851693631     0.1479E-10     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195128   9   0.704416610872  17   1.682695299885  25   3.175416260672
   2  -1.277667044958  10   0.746260122921  18   1.804186820060  26   3.209661156325
   3  -0.898849406810  11   1.155862024397  19   1.902641648752  27   3.328173215213
   4  -0.629870222684  12   1.170770887084  20   2.148883457177  28   3.721936473482
   5  -0.541641772841  13   1.267961746557  21   2.200395756979  29   3.985492632598
   6  -0.485872762812  14   1.449847537230  22   2.540321860021
   7   0.159756317424  15   1.463234913441  23   2.541517310547
   8   0.229311906285  16   1.474444394533  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.41700
     Total cpu time (sec):               0.70116


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.2776E-15

     The smallest diagonal after decomposition is:  -0.5107E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - MLCC2 orbital partitioning:

     Orbital type: cholesky

     Number occupied cc2 orbitals:    6
     Number virtual cc2 orbitals:    23

     Number occupied ccs orbitals:    0
     Number virtual ccs orbitals:     0

  Warning: no ccs orbitals in mlcc2 calculation, recomended to run standard 
           cc2 code.


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-09
     Energy threshold:          0.10E-09

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931609     0.3734E-01     0.7908E+02
    2           -79.085431671929     0.7567E-02     0.1190E-02
    3           -79.085759807021     0.9940E-03     0.3281E-03
    4           -79.085768585650     0.1833E-03     0.8779E-05
    5           -79.085769937826     0.3615E-04     0.1352E-05
    6           -79.085769725742     0.6004E-05     0.2121E-06
    7           -79.085769729164     0.2063E-05     0.3422E-08
    8           -79.085769729230     0.3646E-06     0.6527E-10
    9           -79.085769729230     0.3393E-07     0.9095E-12
   10           -79.085769729553     0.4633E-08     0.3225E-09
   11           -79.085769729586     0.9303E-09     0.3266E-10
   12           -79.085769729575     0.2416E-09     0.1073E-10
   13           -79.085769729575     0.4926E-10     0.4547E-12
  ---------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.085769729575

     Correlation energy (a.u.):           -0.241918035945

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5       -0.015071367757
       14      4       -0.009516336774
        7      4       -0.008547796586
       15      5       -0.006232215141
        5      6       -0.005875107053
        6      2        0.005220429224
       13      5       -0.005212699580
        2      4        0.005071084624
       11      6       -0.003616248568
        4      5        0.003233309237
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007125912543

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.11000
     Total cpu time (sec):               0.20856


  3) Calculation of the excited state (davidson algorithm)
     Calculating left vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  left

     Energy threshold:                0.10E-09
     Residual threshold:              0.10E-09

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.375003176971    0.000000000000     0.7648E+00   0.3750E+00
     2   0.446770340082    0.000000000000     0.7700E+00   0.4468E+00
     3   0.480653291065    0.000000000000     0.7375E+00   0.4807E+00
     4   0.534883245347    0.000000000000     0.7147E+00   0.5349E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.248225466317    0.000000000000     0.6983E-01   0.1268E+00
     2   0.316924920435    0.000000000000     0.9269E-01   0.1298E+00
     3   0.358973721314    0.000000000000     0.9232E-01   0.1217E+00
     4   0.423746119543    0.000000000000     0.7857E-01   0.1111E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246713022305    0.000000000000     0.1415E-01   0.1512E-02
     2   0.314167586568    0.000000000000     0.2239E-01   0.2757E-02
     3   0.356176380344    0.000000000000     0.3573E-01   0.2797E-02
     4   0.420867536898    0.000000000000     0.3664E-01   0.2879E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246053795172    0.000000000000     0.5992E-02   0.6592E-03
     2   0.312593942763    0.000000000000     0.8565E-02   0.1574E-02
     3   0.354260331471    0.000000000000     0.9812E-02   0.1916E-02
     4   0.418885721036    0.000000000000     0.2344E-01   0.1982E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983162796    0.000000000000     0.1007E-02   0.7063E-04
     2   0.312475305881    0.000000000000     0.1782E-02   0.1186E-03
     3   0.354294711889    0.000000000000     0.3158E-02   0.3438E-04
     4   0.417948365253    0.000000000000     0.2091E-01   0.9374E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245982702760    0.000000000000     0.1610E-03   0.4600E-06
     2   0.312488663490    0.000000000000     0.3071E-03   0.1336E-04
     3   0.354252222652    0.000000000000     0.1404E-02   0.4249E-04
     4   0.417982852825    0.000000000000     0.8271E-02   0.3449E-04
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983476548    0.000000000000     0.2906E-04   0.7738E-06
     2   0.312493147441    0.000000000000     0.4796E-04   0.4484E-05
     3   0.354258116024    0.000000000000     0.3000E-03   0.5893E-05
     4   0.418062899141    0.000000000000     0.2490E-02   0.8005E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983378684    0.000000000000     0.6673E-05   0.9786E-07
     2   0.312493132891    0.000000000000     0.9134E-05   0.1455E-07
     3   0.354255425339    0.000000000000     0.1035E-03   0.2691E-05
     4   0.418011295533    0.000000000000     0.6987E-03   0.5160E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983431510    0.000000000000     0.9521E-06   0.5283E-07
     2   0.312493128878    0.000000000000     0.1714E-05   0.4012E-08
     3   0.354257069619    0.000000000000     0.2462E-04   0.1644E-05
     4   0.418024123474    0.000000000000     0.1906E-03   0.1283E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441151    0.000000000000     0.1267E-06   0.9641E-08
     2   0.312493140515    0.000000000000     0.1994E-06   0.1164E-07
     3   0.354257062474    0.000000000000     0.5800E-05   0.7145E-08
     4   0.418025284933    0.000000000000     0.3986E-04   0.1161E-05
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441298    0.000000000000     0.3319E-07   0.1467E-09
     2   0.312493140941    0.000000000000     0.2913E-07   0.4255E-09
     3   0.354257063457    0.000000000000     0.1632E-05   0.9833E-09
     4   0.418025291250    0.000000000000     0.1083E-04   0.6317E-08
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441323    0.000000000000     0.4649E-08   0.2535E-10
     2   0.312493140813    0.000000000000     0.3806E-08   0.1279E-09
     3   0.354257069642    0.000000000000     0.2991E-06   0.6185E-08
     4   0.418025318960    0.000000000000     0.1824E-05   0.2771E-07
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441337    0.000000000000     0.1402E-08   0.1363E-10
     2   0.312493140822    0.000000000000     0.6902E-09   0.9348E-11
     3   0.354257072817    0.000000000000     0.4745E-07   0.3175E-08
     4   0.418025355439    0.000000000000     0.3639E-06   0.3648E-07
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.1945E-09   0.1880E-11
     2   0.312493140823    0.000000000000     0.1341E-09   0.2728E-12
     3   0.354257072950    0.000000000000     0.1080E-07   0.1329E-09
     4   0.418025355038    0.000000000000     0.7603E-07   0.4015E-09
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3608E-10   0.4013E-12
     2   0.312493140823    0.000000000000     0.2656E-10   0.1648E-12
     3   0.354257072889    0.000000000000     0.2108E-08   0.6043E-10
     4   0.418025354395    0.000000000000     0.1738E-07   0.6427E-09
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   62

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3560E-10   0.2248E-14
     2   0.312493140823    0.000000000000     0.2408E-10   0.4940E-14
     3   0.354257072920    0.000000000000     0.3977E-09   0.3052E-10
     4   0.418025354516    0.000000000000     0.3482E-08   0.1206E-09
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3554E-10   0.7216E-15
     2   0.312493140823    0.000000000000     0.2395E-10   0.7605E-14
     3   0.354257072915    0.000000000000     0.8882E-10   0.4739E-11
     4   0.418025354495    0.000000000000     0.6967E-09   0.2016E-10
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   65

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3554E-10   0.8882E-15
     2   0.312493140823    0.000000000000     0.2378E-10   0.1277E-14
     3   0.354257072915    0.000000000000     0.8881E-10   0.1055E-14
     4   0.418025354496    0.000000000000     0.1303E-09   0.5415E-12
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   66

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3556E-10   0.8882E-15
     2   0.312493140823    0.000000000000     0.2397E-10   0.4885E-14
     3   0.354257072915    0.000000000000     0.8868E-10   0.2220E-15
     4   0.418025354495    0.000000000000     0.2164E-10   0.7322E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.245983441338
     Fraction singles (|L1|/|L|):       0.953889445593

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      6        0.947528541058
        4      6       -0.099210006276
        6      6       -0.031035586645
       13      6       -0.027595640176
        1      3        0.011333436525
        1      5       -0.007937837107
       22      6        0.006978571365
       19      5        0.006858621935
        9      6       -0.006535455657
       20      4       -0.006150489569
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312493140823
     Fraction singles (|L1|/|L|):       0.957534890990

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        2      6       -0.943311821407
        3      6       -0.149632341439
        7      6       -0.063475838789
       14      6       -0.018647231561
        2      3       -0.011357579793
       12      6        0.005737886801
        2      5        0.005443615091
        8      2        0.005000213003
        5      4       -0.004440542569
       11      4        0.003126984131
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.354257072915
     Fraction singles (|L1|/|L|):       0.951991795957

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      5        0.935418237059
        2      4        0.127644687426
        4      5       -0.070552810413
        1      2        0.066599652791
        3      4        0.048365859310
        5      6       -0.034411931477
       13      5       -0.026224481879
        7      4        0.022323057788
        4      2       -0.016526729014
       10      5       -0.011797294712
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.418025354495
     Fraction singles (|L1|/|L|):       0.961685700990

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        2      5        0.925531112254
        1      4        0.221302302360
        3      5        0.101177335801
        7      5        0.062933529348
        4      4       -0.058992792931
        2      2        0.026865171664
        8      6       -0.014170871746
       15      4       -0.011856346749
       20      6       -0.009349940430
       10      4        0.007732886794
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.245983441338        6.693550376777
        2                  0.312493140823        8.503371483534
        3                  0.354257072915        9.639825961412
        4                  0.418025354495       11.375049287317
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (left)

     Total wall time (sec):              0.73000
     Total cpu time (sec):               1.16361

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.86300
     Total cpu time (sec):               1.40943

  :: There was 1 warning during the execution of eT. ::

  eT terminated successfully!
