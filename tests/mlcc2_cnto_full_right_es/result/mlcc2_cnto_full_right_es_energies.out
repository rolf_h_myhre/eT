


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es

     mlcc
        cc2 orbitals: cnto-approx
        cnto occupied cc2: 6
        cnto virtual cc2: 23
        cnto states: {1,2,3,4}
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.10800
     Total cpu time (sec):               0.17193


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7377E-10
    10           -78.843851693631     0.1594E-07     0.4405E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.5684E-13
    13           -78.843851693631     0.4264E-09     0.7105E-13
    14           -78.843851693631     0.1280E-09     0.4263E-13
    15           -78.843851693631     0.1479E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195127   9   0.704416610872  17   1.682695299885  25   3.175416260672
   2  -1.277667044958  10   0.746260122921  18   1.804186820060  26   3.209661156325
   3  -0.898849406810  11   1.155862024397  19   1.902641648752  27   3.328173215213
   4  -0.629870222684  12   1.170770887084  20   2.148883457177  28   3.721936473482
   5  -0.541641772841  13   1.267961746557  21   2.200395756979  29   3.985492632598
   6  -0.485872762812  14   1.449847537230  22   2.540321860021
   7   0.159756317424  15   1.463234913441  23   2.541517310547
   8   0.229311906285  16   1.474444394533  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.41600
     Total cpu time (sec):               0.70295


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

  Running CCS calculation for NTOs/CNTOs.

  - Summary of CCS calculation for NTOs/CNTOs:

     Wall time for CCS ground calculation (sec):                   0.01
     CPU time for CCS ground calculation (sec):                    0.02

     Wall time for CCS excited calculation (sec):                  0.06
     CPU time for CCS excited calculation (sec):                   0.12

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - MLCC2 orbital partitioning:

     Orbital type: cnto-approx

     Number occupied cc2 orbitals:    6
     Number virtual cc2 orbitals:    23

     Number occupied ccs orbitals:    0
     Number virtual ccs orbitals:     0

  Warning: no ccs orbitals in mlcc2 calculation, recomended to run standard 
           cc2 code.


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-09
     Energy threshold:          0.10E-09

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931609     0.3734E-01     0.7908E+02
    2           -79.085431671929     0.7567E-02     0.1190E-02
    3           -79.085759807021     0.9940E-03     0.3281E-03
    4           -79.085768585650     0.1833E-03     0.8779E-05
    5           -79.085769937826     0.3615E-04     0.1352E-05
    6           -79.085769725742     0.6004E-05     0.2121E-06
    7           -79.085769729164     0.2063E-05     0.3422E-08
    8           -79.085769729230     0.3646E-06     0.6527E-10
    9           -79.085769729230     0.3393E-07     0.9095E-12
   10           -79.085769729553     0.4633E-08     0.3225E-09
   11           -79.085769729586     0.9303E-09     0.3266E-10
   12           -79.085769729575     0.2416E-09     0.1073E-10
   13           -79.085769729575     0.4926E-10     0.4547E-12
  ---------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.085769729575

     Correlation energy (a.u.):           -0.241918035945

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.015071367757
       14      4        0.009516336774
        7      4       -0.008547796586
       15      5        0.006232215141
        5      6        0.005875107053
        6      2        0.005220429224
       13      5       -0.005212699580
        2      4        0.005071084624
       11      6       -0.003616248568
        4      5       -0.003233309237
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007125912543

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.10900
     Total cpu time (sec):               0.20541


  3) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-09
     Residual threshold:              0.10E-09

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.375003176971    0.000000000000     0.4812E+00   0.3750E+00
     2   0.446770340081    0.000000000000     0.4843E+00   0.4468E+00
     3   0.480653291065    0.000000000000     0.4579E+00   0.4807E+00
     4   0.534883245347    0.000000000000     0.4555E+00   0.5349E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.246650345585    0.000000000000     0.5065E-01   0.1284E+00
     2   0.314083924343    0.000000000000     0.6604E-01   0.1327E+00
     3   0.357502456641    0.000000000000     0.6719E-01   0.1232E+00
     4   0.422300873730    0.000000000000     0.6150E-01   0.1126E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245809384236    0.000000000000     0.1666E-01   0.8410E-03
     2   0.312283030702    0.000000000000     0.2718E-01   0.1801E-02
     3   0.353929276201    0.000000000000     0.3102E-01   0.3573E-02
     4   0.419608577446    0.000000000000     0.3189E-01   0.2692E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245929213051    0.000000000000     0.4755E-02   0.1198E-03
     2   0.312438517456    0.000000000000     0.6870E-02   0.1555E-03
     3   0.354451561264    0.000000000000     0.7456E-02   0.5223E-03
     4   0.419315824895    0.000000000000     0.1744E-01   0.2928E-03
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245990306934    0.000000000000     0.8154E-03   0.6109E-04
     2   0.312522740436    0.000000000000     0.1664E-02   0.8422E-04
     3   0.354228820877    0.000000000000     0.3070E-02   0.2227E-03
     4   0.418705404180    0.000000000000     0.2062E-01   0.6104E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983821636    0.000000000000     0.1529E-03   0.6485E-05
     2   0.312495753540    0.000000000000     0.2603E-03   0.2699E-04
     3   0.354261514659    0.000000000000     0.1084E-02   0.3269E-04
     4   0.418093420698    0.000000000000     0.6916E-02   0.6120E-03
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983343003    0.000000000000     0.3147E-04   0.4786E-06
     2   0.312492854227    0.000000000000     0.4546E-04   0.2899E-05
     3   0.354258452010    0.000000000000     0.2782E-03   0.3063E-05
     4   0.418005210586    0.000000000000     0.2277E-02   0.8821E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983491731    0.000000000000     0.5527E-05   0.1487E-06
     2   0.312493147787    0.000000000000     0.7314E-05   0.2936E-06
     3   0.354258690302    0.000000000000     0.8241E-04   0.2383E-06
     4   0.418037806913    0.000000000000     0.5787E-03   0.3260E-04
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983447316    0.000000000000     0.7691E-06   0.4441E-07
     2   0.312493151399    0.000000000000     0.1420E-05   0.3611E-08
     3   0.354257035476    0.000000000000     0.2198E-04   0.1655E-05
     4   0.418026102302    0.000000000000     0.1607E-03   0.1170E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441291    0.000000000000     0.1099E-06   0.6025E-08
     2   0.312493141276    0.000000000000     0.1652E-06   0.1012E-07
     3   0.354257116563    0.000000000000     0.5008E-05   0.8109E-07
     4   0.418025380597    0.000000000000     0.3546E-04   0.7217E-06
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441327    0.000000000000     0.2709E-07   0.3576E-10
     2   0.312493140741    0.000000000000     0.2567E-07   0.5344E-09
     3   0.354257087590    0.000000000000     0.1317E-05   0.2897E-07
     4   0.418025407581    0.000000000000     0.8403E-05   0.2698E-07
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441353    0.000000000000     0.4759E-08   0.2622E-10
     2   0.312493140825    0.000000000000     0.3361E-08   0.8377E-10
     3   0.354257074116    0.000000000000     0.2693E-06   0.1347E-07
     4   0.418025383345    0.000000000000     0.1469E-05   0.2424E-07
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441340    0.000000000000     0.1116E-08   0.1257E-10
     2   0.312493140824    0.000000000000     0.6293E-09   0.1451E-11
     3   0.354257072980    0.000000000000     0.4420E-07   0.1135E-08
     4   0.418025352617    0.000000000000     0.2854E-06   0.3073E-07
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.1734E-09   0.2602E-11
     2   0.312493140823    0.000000000000     0.1172E-09   0.4759E-12
     3   0.354257072987    0.000000000000     0.9100E-08   0.6721E-11
     4   0.418025354388    0.000000000000     0.6563E-07   0.1772E-08
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3235E-10   0.3737E-12
     2   0.312493140823    0.000000000000     0.2302E-10   0.2792E-12
     3   0.354257072929    0.000000000000     0.1841E-08   0.5798E-10
     4   0.418025354556    0.000000000000     0.1417E-07   0.1678E-09
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   62

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3194E-10   0.1887E-14
     2   0.312493140823    0.000000000000     0.2093E-10   0.5107E-14
     3   0.354257072914    0.000000000000     0.3456E-09   0.1505E-10
     4   0.418025354491    0.000000000000     0.2915E-08   0.6482E-10
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3193E-10   0.8049E-15
     2   0.312493140823    0.000000000000     0.2077E-10   0.9437E-15
     3   0.354257072916    0.000000000000     0.8514E-10   0.1608E-11
     4   0.418025354493    0.000000000000     0.5756E-09   0.1669E-11
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   65

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3193E-10   0.1804E-14
     2   0.312493140823    0.000000000000     0.2072E-10   0.3109E-14
     3   0.354257072916    0.000000000000     0.8504E-10   0.2776E-15
     4   0.418025354495    0.000000000000     0.1136E-09   0.1993E-11
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   66

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.245983441338    0.000000000000     0.3193E-10   0.1721E-14
     2   0.312493140823    0.000000000000     0.2062E-10   0.1610E-14
     3   0.354257072916    0.000000000000     0.8498E-10   0.1887E-14
     4   0.418025354495    0.000000000000     0.1995E-10   0.2439E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.245983441338
     Fraction singles (|R1|/|R|):       0.980304305799

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6       -0.973021602897
        4      6        0.106789400091
        6      6       -0.036115666383
       13      6       -0.029969632228
        1      3       -0.011639320543
       22      6       -0.007968761285
        1      5       -0.007878549048
       19      5       -0.007714058162
        9      6        0.007315563575
       20      4       -0.006976576961
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.312493140823
     Fraction singles (|R1|/|R|):       0.981441033026

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6       -0.965628997075
        3      6       -0.158512867310
        7      6       -0.070134692770
       14      6        0.021348644737
        2      3       -0.011607275500
       12      6        0.006210517824
        2      5       -0.005382310336
        8      2        0.005308524736
        5      4        0.004510972657
       11      4        0.003272191868
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.354257072916
     Fraction singles (|R1|/|R|):       0.983257567057

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5       -0.965285743631
        2      4        0.132449574661
        4      5        0.076819133270
        1      2       -0.068258196365
        3      4        0.051829802723
        5      6        0.039876044691
       13      5       -0.028798153831
        7      4        0.026087665166
        4      2        0.018477884698
       10      5       -0.013058391621
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.418025354495
     Fraction singles (|R1|/|R|):       0.982264305191

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      5       -0.947047794654
        1      4        0.214995854402
        3      5       -0.108409956416
        7      5       -0.069257336144
        4      4       -0.060000555568
        2      2       -0.023818797680
        8      6        0.015066880566
       15      4       -0.012597150542
       20      6        0.010167354109
       14      5        0.009501846971
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.245983441338        6.693550376775
        2                  0.312493140823        8.503371483534
        3                  0.354257072916        9.639825961423
        4                  0.418025354495       11.375049287321
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (right)

     Total wall time (sec):              0.71300
     Total cpu time (sec):               1.09259

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.92300
     Total cpu time (sec):               1.49016

  :: There was 1 warning during the execution of eT. ::

  eT terminated successfully!
