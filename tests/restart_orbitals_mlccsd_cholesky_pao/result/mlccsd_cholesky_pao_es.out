


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     method
        hf
        mlccsd
     end method

     solver cc gs
        omega threshold:  1.0d-12
        energy threshold: 1.0d-12
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-12
        energy threshold:   1.0d-12
        right eigenvectors
     end solver cc es

     active atoms
        selection type: list
        ccsd: {3}
     end active atoms

     mlcc
        levels: ccs, ccsd
        ccsd orbitals: cholesky-pao
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        3 O             cc-pvdz    ccsd
     ====================================
     Total number of active atoms: 1
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.075790000000     5.000000000000        3
        2 H      0.866810000000     0.601440000000     5.000000000000        1
        3 H     -0.866810000000     0.601440000000     5.000000000000        2
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.143222342981     9.448630622825        3
        2 H      1.638033502034     1.136556880358     9.448630622825        1
        3 H     -1.638033502034     1.136556880358     9.448630622825        2
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-11
     Gradient threshold:            0.1000E-11

     Coulomb screening threshold:   0.1000E-17
     Exchange screening threshold:  0.1000E-15
     Fock precision:                0.1000E-35
     Integral cutoff:               0.1000E-17

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836315
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592587     0.9053E-01     0.7880E+02
     2           -78.828675852655     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846800     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7053E-09
     9           -78.843851693630     0.2071E-06     0.7378E-10
    10           -78.843851693631     0.1594E-07     0.3411E-12
    11           -78.843851693631     0.3322E-08     0.0000E+00
    12           -78.843851693631     0.1197E-08     0.1421E-13
    13           -78.843851693631     0.4264E-09     0.7105E-13
    14           -78.843851693631     0.1280E-09     0.7105E-13
    15           -78.843851693631     0.1479E-10     0.4263E-13
    16           -78.843851693631     0.2908E-11     0.0000E+00
    17           -78.843851693631     0.6423E-12     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080251
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195142   9   0.704416610867  17   1.682695299878  25   3.175416260660
   2  -1.277667044967  10   0.746260122915  18   1.804186820053  26   3.209661156313
   3  -0.898849406811  11   1.155862024381  19   1.902641648743  27   3.328173215200
   4  -0.629870222687  12   1.170770887076  20   2.148883457169  28   3.721936473470
   5  -0.541641772852  13   1.267961746553  21   2.200395756970  29   3.985492632586
   6  -0.485872762834  14   1.449847537220  22   2.540321860021
   7   0.159756317417  15   1.463234913437  23   2.541517310547
   8   0.229311906285  16   1.474444394527  24   2.559338707349
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.77300
     Total cpu time (sec):               1.40379


  :: MLCCSD wavefunction
  =========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.38800
     Total cpu time (sec):               0.61920


  2) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.1110E-15

  - MLCCSD orbital partitioning:

     Orbital type: cholesky-pao

     Number occupied ccsd orbitals:    5
     Number virtual ccsd orbitals:    13

     Number occupied cc2 orbitals:     0
     Number virtual cc2 orbitals:      0

     Number occupied ccs orbitals:     1
     Number virtual ccs orbitals:     10


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-11
     Energy threshold:          0.10E-11

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.044160688176     0.7984E-01     0.7904E+02
    2           -79.048084138476     0.2642E-01     0.3923E-02
    3           -79.053442934922     0.8503E-02     0.5359E-02
    4           -79.054587871610     0.2277E-02     0.1145E-02
    5           -79.054560413514     0.7812E-03     0.2746E-04
    6           -79.054607772130     0.3314E-03     0.4736E-04
    7           -79.054606049427     0.1193E-03     0.1723E-05
    8           -79.054600210232     0.4802E-04     0.5839E-05
    9           -79.054599449047     0.1421E-04     0.7612E-06
   10           -79.054598958575     0.4396E-05     0.4905E-06
   11           -79.054599039880     0.1820E-05     0.8130E-07
   12           -79.054599099957     0.8952E-06     0.6008E-07
   13           -79.054599135911     0.3653E-06     0.3595E-07
   14           -79.054599136993     0.1403E-06     0.1082E-08
   15           -79.054599133583     0.4410E-07     0.3410E-08
   16           -79.054599132423     0.1566E-07     0.1160E-08
   17           -79.054599132674     0.4060E-08     0.2509E-09
   18           -79.054599132671     0.1382E-08     0.3510E-11
   19           -79.054599132619     0.5214E-09     0.5136E-10
   20           -79.054599132612     0.1664E-09     0.7873E-11
   21           -79.054599132609     0.5738E-10     0.2899E-11
   22           -79.054599132610     0.2601E-10     0.1421E-11
   23           -79.054599132611     0.1251E-10     0.1009E-11
   24           -79.054599132611     0.5371E-11     0.3268E-12
   25           -79.054599132611     0.1992E-11     0.8527E-13
   26           -79.054599132611     0.9635E-12     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 26 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.054599132611

     Correlation energy (a.u.):           -0.210747438980

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      4        0.013231738418
        5      3       -0.012538085356
        8      4        0.006327515320
        6      2        0.005258843924
        4      5        0.004821055107
        6      4        0.004749918913
       12      3       -0.004360820324
        8      2        0.003751919891
        2      3       -0.003424627434
       23      3        0.003380814321
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.006745797828

  - Finished solving the MLCCSD ground state equations

     Total wall time (sec):              0.33300
     Total cpu time (sec):               0.60116


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-11
     Residual threshold:              0.10E-11

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.452785418439    0.000000000000     0.5612E+00   0.4528E+00
     2   0.587472416313    0.000000000000     0.5553E+00   0.5875E+00
     3   0.587719368789    0.000000000000     0.5586E+00   0.5877E+00
     4   0.639373271161    0.000000000000     0.5045E+00   0.6394E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.281289602494    0.000000000000     0.1835E+00   0.1715E+00
     2   0.386011051111    0.000000000000     0.2392E+00   0.2015E+00
     3   0.396586267903    0.000000000000     0.2046E+00   0.1911E+00
     4   0.498248015622    0.000000000000     0.1858E+00   0.1411E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263707556277    0.000000000000     0.6492E-01   0.1758E-01
     2   0.357915633314    0.000000000000     0.8933E-01   0.2810E-01
     3   0.372338335779    0.000000000000     0.7859E-01   0.2425E-01
     4   0.478686867783    0.000000000000     0.7829E-01   0.1956E-01
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263879289553    0.000000000000     0.2382E-01   0.1717E-03
     2   0.356531786967    0.000000000000     0.1539E-01   0.1384E-02
     3   0.371986431933    0.000000000000     0.2950E-01   0.3519E-03
     4   0.477050576296    0.000000000000     0.4182E-01   0.1636E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263657209848    0.000000000000     0.1052E-01   0.2221E-03
     2   0.356267493675    0.000000000000     0.4891E-02   0.2643E-03
     3   0.371606839875    0.000000000000     0.1209E-01   0.3796E-03
     4   0.474516479656    0.000000000000     0.4781E-01   0.2534E-02
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263187094337    0.000000000000     0.3446E-02   0.4701E-03
     2   0.356242100843    0.000000000000     0.1512E-02   0.2539E-04
     3   0.371147055446    0.000000000000     0.3740E-02   0.4598E-03
     4   0.466080049394    0.000000000000     0.8382E-01   0.8436E-02
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263244895197    0.000000000000     0.7843E-03   0.5780E-04
     2   0.356241287770    0.000000000000     0.4361E-03   0.8131E-06
     3   0.371212655680    0.000000000000     0.1261E-02   0.6560E-04
     4   0.458334014010    0.000000000000     0.7899E-01   0.7746E-02
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263250834532    0.000000000000     0.2372E-03   0.5939E-05
     2   0.356241154215    0.000000000000     0.9961E-04   0.1336E-06
     3   0.371241793373    0.000000000000     0.5163E-03   0.2914E-04
     4   0.454823976110    0.000000000000     0.2312E-01   0.3510E-02
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263248145995    0.000000000000     0.6739E-04   0.2689E-05
     2   0.356241309787    0.000000000000     0.2215E-04   0.1556E-06
     3   0.371227574759    0.000000000000     0.2343E-03   0.1422E-04
     4   0.454501785333    0.000000000000     0.6761E-02   0.3222E-03
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246400749    0.000000000000     0.2059E-04   0.1745E-05
     2   0.356241327993    0.000000000000     0.5679E-05   0.1821E-07
     3   0.371225103000    0.000000000000     0.1109E-03   0.2472E-05
     4   0.454482578629    0.000000000000     0.2101E-02   0.1921E-04
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246301591    0.000000000000     0.4869E-05   0.9916E-07
     2   0.356241310268    0.000000000000     0.2069E-05   0.1773E-07
     3   0.371227400560    0.000000000000     0.5317E-04   0.2298E-05
     4   0.454506048909    0.000000000000     0.6800E-03   0.2347E-04
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246315691    0.000000000000     0.1298E-05   0.1410E-07
     2   0.356241304810    0.000000000000     0.6885E-06   0.5458E-08
     3   0.371227158328    0.000000000000     0.1854E-04   0.2422E-06
     4   0.454501536768    0.000000000000     0.1856E-03   0.4512E-05
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246326444    0.000000000000     0.2782E-06   0.1075E-07
     2   0.356241305716    0.000000000000     0.2019E-06   0.9054E-09
     3   0.371227068020    0.000000000000     0.5350E-05   0.9031E-07
     4   0.454501042358    0.000000000000     0.5918E-04   0.4944E-06
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328291    0.000000000000     0.5257E-07   0.1847E-08
     2   0.356241306421    0.000000000000     0.4594E-07   0.7049E-09
     3   0.371227030149    0.000000000000     0.1320E-05   0.3787E-07
     4   0.454501002934    0.000000000000     0.1749E-04   0.3942E-07
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328288    0.000000000000     0.1101E-07   0.3101E-11
     2   0.356241306538    0.000000000000     0.1140E-07   0.1169E-09
     3   0.371227026665    0.000000000000     0.3465E-06   0.3484E-08
     4   0.454501005135    0.000000000000     0.5064E-05   0.2201E-08
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328330    0.000000000000     0.2303E-08   0.4224E-10
     2   0.356241306557    0.000000000000     0.2813E-08   0.1954E-10
     3   0.371227026380    0.000000000000     0.8245E-07   0.2850E-09
     4   0.454501014923    0.000000000000     0.1528E-05   0.9788E-08
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   68

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328319    0.000000000000     0.5056E-09   0.1083E-10
     2   0.356241306559    0.000000000000     0.6873E-09   0.2043E-11
     3   0.371227026588    0.000000000000     0.2176E-07   0.2081E-09
     4   0.454501007465    0.000000000000     0.3606E-06   0.7458E-08
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   72

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.1304E-09   0.3736E-11
     2   0.356241306559    0.000000000000     0.2166E-09   0.5945E-12
     3   0.371227026645    0.000000000000     0.7013E-08   0.5666E-10
     4   0.454501009041    0.000000000000     0.8542E-07   0.1576E-08
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   76

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.3163E-10   0.4589E-12
     2   0.356241306558    0.000000000000     0.6040E-10   0.2278E-12
     3   0.371227026643    0.000000000000     0.1922E-08   0.2388E-11
     4   0.454501009447    0.000000000000     0.1856E-07   0.4055E-09
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   80

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.7870E-11   0.4663E-14
     2   0.356241306558    0.000000000000     0.1434E-10   0.1427E-13
     3   0.371227026640    0.000000000000     0.4343E-09   0.2790E-11
     4   0.454501009336    0.000000000000     0.3786E-08   0.1105E-09
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   84

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.1865E-11   0.5662E-14
     2   0.356241306558    0.000000000000     0.3197E-11   0.1016E-13
     3   0.371227026639    0.000000000000     0.9140E-10   0.4116E-12
     4   0.454501009368    0.000000000000     0.9105E-09   0.3186E-10
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   88

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.3471E-12   0.1332E-13
     2   0.356241306558    0.000000000000     0.6780E-12   0.1682E-13
     3   0.371227026640    0.000000000000     0.2007E-10   0.2093E-12
     4   0.454501009362    0.000000000000     0.2336E-09   0.5584E-11
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   90

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.3453E-12   0.3997E-14
     2   0.356241306558    0.000000000000     0.1646E-12   0.4718E-14
     3   0.371227026640    0.000000000000     0.4804E-11   0.3553E-13
     4   0.454501009362    0.000000000000     0.6791E-10   0.1137E-12
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   92

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.3426E-12   0.1554E-14
     2   0.356241306558    0.000000000000     0.1022E-12   0.1388E-14
     3   0.371227026640    0.000000000000     0.1079E-11   0.1849E-13
     4   0.454501009362    0.000000000000     0.1646E-10   0.2653E-13
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   94

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.3489E-12   0.3220E-14
     2   0.356241306558    0.000000000000     0.1029E-12   0.8327E-15
     3   0.371227026640    0.000000000000     0.2324E-12   0.3941E-14
     4   0.454501009362    0.000000000000     0.3802E-11   0.7050E-14
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   95

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263246328315    0.000000000000     0.3469E-12   0.4441E-14
     2   0.356241306558    0.000000000000     0.1018E-12   0.9437E-15
     3   0.371227026640    0.000000000000     0.1664E-12   0.1110E-14
     4   0.454501009362    0.000000000000     0.8168E-12   0.1049E-13
  ------------------------------------------------------------------------
  Convergence criterion met in 26 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.263246328315
     Fraction singles (|R1|/|R|):       0.978828499645

     MLCC diagnostics:

     |R1^internal|/|R| =       0.948923567278
     |R1^internal|/|R1| =      0.969448241058

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      5       -0.946089983451
       16      5        0.214767827313
       15      5        0.105647869411
       10      5       -0.049206120039
        3      5        0.029102030032
        6      5       -0.028354755157
       13      5       -0.023302452209
        8      5        0.019610562029
       19      5        0.011375603491
        1      6       -0.011166609338
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.356241306558
     Fraction singles (|R1|/|R|):       0.982910773156

     MLCC diagnostics:

     |R1^internal|/|R| =       0.879339534911
     |R1^internal|/|R1| =      0.894628036366

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      5        0.861047787331
       14      5        0.412602640299
        5      5        0.160653661805
       23      5        0.142533411910
       12      5       -0.076888852440
       18      5        0.044357650272
       22      5        0.011964400292
        2      6        0.009330702180
       14      6        0.007334128768
       17      2        0.006677859317
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.371227026640
     Fraction singles (|R1|/|R|):       0.984354373891

     MLCC diagnostics:

     |R1^internal|/|R| =       0.948338551661
     |R1^internal|/|R1| =      0.963411731400

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      4       -0.927804007527
       16      4        0.231796190073
        2      3        0.142022559000
       15      4        0.113903781255
        1      2        0.073746092626
        6      4       -0.070394816031
       10      4       -0.048325799596
       14      3        0.043138033248
        4      5        0.042114142595
       13      4       -0.039257832263
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.454501009362
     Fraction singles (|R1|/|R|):       0.982904662325

     MLCC diagnostics:

     |R1^internal|/|R| =       0.881342393071
     |R1^internal|/|R1| =      0.896671291584

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      4       -0.722142582254
        1      3        0.473304971490
       14      4       -0.394906800869
        5      4       -0.147874125686
       23      4       -0.129048540912
       16      3       -0.110840479851
       12      4        0.083638224317
       15      3       -0.052486163934
       18      4       -0.037998865971
       10      3        0.034348823792
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.263246328315        7.163297458125
        2                  0.356241306558        9.693819709027
        3                  0.371227026640       10.101601922949
        4                  0.454501009362       12.367602412242
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCCSD excited state equations (right)

     Total wall time (sec):              1.05000
     Total cpu time (sec):               1.76626

  - Timings for the MLCCSD excited state calculation

     Total wall time (sec):              1.78700
     Total cpu time (sec):               3.01859

  eT terminated successfully!
