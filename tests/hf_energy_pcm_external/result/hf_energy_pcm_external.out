


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O
        charge: 0
     end system

     do
       ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     pcm
        input : external
     end pcm

     method
        hf
     end method


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         24
     Cartesian basis functions:    25
     Primitive basis functions:    49

     Nuclear repulsion energy (a.u.):              9.307879526626
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.009319000000     1.133156000000     0.000000000000        1
        2 H      0.023452000000     0.185621000000     0.000000000000        2
        3 H      0.906315000000     1.422088000000     0.000000000000        3
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.017610357755     2.141354496408     0.000000000000        1
        2 H      0.044317857073     0.350772852968     0.000000000000        2
        3 H      1.712687132585     2.687356845030     0.000000000000        3
     ==============================================================================
     ~~~~~~~~~~ PCMSolver ~~~~~~~~~~
     Using CODATA 2010 set of constants.
     Input parsing done API-side
     ========== Cavity
     Atomic radii set:
     Cavity type: GePol
     Average tesserae area = 0.3 Ang^2
     Solvent probe radius = 1.385 Ang
     Number of spheres = 3 [initial = 3; added = 0]
     Number of finite elements = 192 (0 pruned)
     Number of irreducible finite elements = 192
     ============ Spheres list (in Angstrom)
      Sphere   on   Radius   Alpha       X            Y            Z
     -------- ---- -------- ------- -----------  -----------  -----------
        1      O    1.5200   1.20     0.009319     1.133156     0.000000
        2      H    1.2000   1.20     0.023452     0.185621     0.000000
        3      H    1.2000   1.20     0.906315     1.422088     0.000000
     ========== Static solver
     Solver Type: IEFPCM, isotropic
     PCM matrix hermitivitized
     ============ Medium
     Medium initialized from solvent built-in data.
     Solvent name:          Water
     Static  permittivity = 78.39
     Optical permittivity = 1.78
     Solvent radius =       1.39 Ang
     .... Inside
     Green's function type: vacuum
     .... Outside
     Green's function type: uniform dielectric
     Permittivity = 78.39


  :: RHF wavefunction
  ======================

  This is a PCM calculation

     Polarizable Continuum Solver via PCMSolver

     For details on PCM, see:
     Tomasi, Mennucci, Cammi, Chem. Rev. 2005, 105, 2999-3094.

     For details on PCMSolver, see:
     Di Remigio et al., IJQC, 2019, 119, e25685

     PCM Solver was set via external file

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:         19
     Number of molecular orbitals:       24
     Number of atomic orbitals:          24


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-09
     Gradient threshold:            0.1000E-09

     Coulomb screening threshold:   0.1000E-15
     Exchange screening threshold:  0.1000E-13
     Fock precision:                0.1000E-31
     Integral cutoff:               0.1000E-15

  - Setting initial AO density to SAD

     Energy of initial guess:               -75.916824923907
     Number of electrons in guess:           10.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -76.001683148260     0.8228E-01     0.7600E+02
     2           -76.027650726027     0.4857E-01     0.2597E-01
     3           -76.035332285657     0.5102E-02     0.7682E-02
     4           -76.035487691814     0.9052E-03     0.1554E-03
     5           -76.035497680607     0.1549E-03     0.9989E-05
     6           -76.035497969967     0.3113E-04     0.2894E-06
     7           -76.035497980733     0.1816E-05     0.1077E-07
     8           -76.035497980819     0.6847E-06     0.8625E-10
     9           -76.035497980826     0.1044E-06     0.6480E-11
    10           -76.035497980826     0.1055E-07     0.1421E-12
    11           -76.035497980826     0.8555E-09     0.4263E-13
    12           -76.035497980826     0.1803E-09     0.7105E-13
    13           -76.035497980826     0.6723E-10     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.697569870182
     Nuclear repulsion energy:       9.307879526626
     Electronic energy:            -85.343377507452
     Total energy:                 -76.035497980826

  - Summary of QM/PCM energetics:
                                         a.u.             eV     kcal/mol
     QM/PCM SCF Contribution:       -0.009387191615
     QM/PCM Electrostatic Energy:   -0.018774383230    -0.51088   -11.781

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.546744331369   7   0.275148310949  13   1.464566153880  19   2.526295693073
   2  -1.340860449892   8   0.811737292078  14   1.479426134170  20   3.280216596208
   3  -0.709505925404   9   0.863779241489  15   1.694808355597  21   3.359419487728
   4  -0.569195794239  10   1.155116551842  16   1.869320703401  22   3.510584475595
   5  -0.496469923735  11   1.194578299681  17   1.941723501361  23   3.901945769601
   6   0.201099946447  12   1.247059869190  18   2.496316625118  24   4.144240039379
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              1.55100
     Total cpu time (sec):               1.94772

  eT terminated successfully!
