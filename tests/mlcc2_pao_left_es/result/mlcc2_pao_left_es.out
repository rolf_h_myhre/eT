


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
        left eigenvectors
     end solver cc es

     active atoms
        selection type: list
        cc2: {3}
     end active atoms

     mlcc
        cc2 orbitals: cholesky-pao
        cholesky threshold: 1.0d-1
     end mlcc


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     Active atoms:

     ====================================
        Atom              Basis   Method
     ====================================
        3 O             cc-pvdz    cc2
     ====================================
     Total number of active atoms: 1
     OBS: Atoms will be reordered, active atoms first

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.075790000000     5.000000000000        3
        2 H      0.866810000000     0.601440000000     5.000000000000        1
        3 H     -0.866810000000     0.601440000000     5.000000000000        2
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 O      0.000000000000    -0.143222342981     9.448630622825        3
        2 H      1.638033502034     1.136556880358     9.448630622825        1
        3 H     -1.638033502034     1.136556880358     9.448630622825        2
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1110E-14

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.11500
     Total cpu time (sec):               0.16872


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Setting initial AO density to SAD

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592593     0.9053E-01     0.7880E+02
     2           -78.828675852657     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846800     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7377E-10
    10           -78.843851693631     0.1594E-07     0.3695E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.7105E-13
    13           -78.843851693631     0.4264E-09     0.4263E-13
    14           -78.843851693631     0.1280E-09     0.4263E-13
    15           -78.843851693631     0.1479E-10     0.1421E-13
    16           -78.843851693631     0.2907E-11     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195145   9   0.704416610867  17   1.682695299877  25   3.175416260660
   2  -1.277667044968  10   0.746260122914  18   1.804186820052  26   3.209661156313
   3  -0.898849406810  11   1.155862024383  19   1.902641648742  27   3.328173215200
   4  -0.629870222690  12   1.170770887076  20   2.148883457168  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756969  29   3.985492632586
   6  -0.485872762832  14   1.449847537220  22   2.540321860021
   7   0.159756317416  15   1.463234913438  23   2.541517310547
   8   0.229311906283  16   1.474444394527  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.45200
     Total cpu time (sec):               0.77307


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Preparation of MO basis and integrals
     2) Calculation of the ground state (diis algorithm)
     3) Calculation of the excited state (davidson algorithm)


  1) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.1010E-15

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - MLCC2 orbital partitioning:

     Orbital type: cholesky-pao

     Number occupied cc2 orbitals:    5
     Number virtual cc2 orbitals:    13

     Number occupied ccs orbitals:    1
     Number virtual ccs orbitals:    10


  2) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-10
     Energy threshold:          0.10E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.044160688177     0.3657E-01     0.7904E+02
    2           -79.045305454073     0.9064E-02     0.1145E-02
    3           -79.045492384052     0.2665E-02     0.1869E-03
    4           -79.045571807811     0.1135E-02     0.7942E-04
    5           -79.045579775786     0.3744E-03     0.7968E-05
    6           -79.045582170050     0.9856E-04     0.2394E-05
    7           -79.045582045317     0.3590E-04     0.1247E-06
    8           -79.045582083702     0.9826E-05     0.3839E-07
    9           -79.045582118491     0.3151E-05     0.3479E-07
   10           -79.045582123523     0.1515E-05     0.5033E-08
   11           -79.045582122561     0.5659E-06     0.9621E-09
   12           -79.045582124104     0.2337E-06     0.1543E-08
   13           -79.045582123441     0.6956E-07     0.6628E-09
   14           -79.045582123617     0.2039E-07     0.1760E-09
   15           -79.045582123477     0.6022E-08     0.1396E-09
   16           -79.045582123484     0.2285E-08     0.6580E-11
   17           -79.045582123500     0.6848E-09     0.1627E-10
   18           -79.045582123509     0.2617E-09     0.8569E-11
   19           -79.045582123512     0.8126E-10     0.3055E-11
   20           -79.045582123512     0.3028E-10     0.4547E-12
   21           -79.045582123512     0.1479E-10     0.1705E-12
   22           -79.045582123512     0.5019E-11     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 22 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.045582123512

     Correlation energy (a.u.):           -0.201730429881

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        5      3       -0.012293837307
        1      4        0.012108647669
        8      4        0.006278792798
        2      3       -0.006237282753
        6      4        0.005856750921
        4      5        0.005710978449
        6      2        0.005318471803
       12      3       -0.004036543512
        8      2        0.003089764537
       23      3        0.002958539743
     ------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.006679786378

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.15100
     Total cpu time (sec):               0.29767


  3) Calculation of the excited state (davidson algorithm)
     Calculating left vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  left

     Energy threshold:                0.10E-10
     Residual threshold:              0.10E-10

     Number of singlet states:               4
     Max number of iterations:             100

     Max reduced space dimension:          100

     Reduced space basis and transforms are stored on disk.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.448022669129    0.000000000000     0.8043E+00   0.4480E+00
     2   0.580591190772    0.000000000000     0.8138E+00   0.5806E+00
     3   0.582257862299    0.000000000000     0.7426E+00   0.5823E+00
     4   0.633142397968    0.000000000000     0.6923E+00   0.6331E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.279739416404    0.000000000000     0.1573E+00   0.1683E+00
     2   0.393889401694    0.000000000000     0.1897E+00   0.1867E+00
     3   0.396021420483    0.000000000000     0.2248E+00   0.1862E+00
     4   0.497466020067    0.000000000000     0.1690E+00   0.1357E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.270439888581    0.000000000000     0.8730E-01   0.9300E-02
     2   0.369932805930    0.000000000000     0.1058E+00   0.2396E-01
     3   0.378712039535    0.000000000000     0.1019E+00   0.1731E-01
     4   0.483569695216    0.000000000000     0.1028E+00   0.1390E-01
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264529817678    0.000000000000     0.2384E-01   0.5910E-02
     2   0.362664440614    0.000000000000     0.1092E-01   0.7268E-02
     3   0.370097985606    0.000000000000     0.2848E-01   0.8614E-02
     4   0.474815626634    0.000000000000     0.3476E-01   0.8754E-02
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.263912729001    0.000000000000     0.1407E-01   0.6171E-03
     2   0.362539678514    0.000000000000     0.3686E-02   0.1248E-03
     3   0.369913235751    0.000000000000     0.1487E-01   0.1847E-03
     4   0.473095343892    0.000000000000     0.4130E-01   0.1720E-02
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264256578180    0.000000000000     0.1957E-02   0.3438E-03
     2   0.362564342259    0.000000000000     0.6926E-03   0.2466E-04
     3   0.370169807430    0.000000000000     0.3337E-02   0.2566E-03
     4   0.472397468033    0.000000000000     0.6643E-01   0.6979E-03
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264233402326    0.000000000000     0.5056E-03   0.2318E-04
     2   0.362558353382    0.000000000000     0.2190E-03   0.5989E-05
     3   0.370083461569    0.000000000000     0.1010E-02   0.8635E-04
     4   0.459303622760    0.000000000000     0.6616E-01   0.1309E-01
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264228927770    0.000000000000     0.1078E-03   0.4475E-05
     2   0.362556292004    0.000000000000     0.5209E-04   0.2061E-05
     3   0.370068172180    0.000000000000     0.2314E-03   0.1529E-04
     4   0.458767026585    0.000000000000     0.1860E-01   0.5366E-03
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264228979637    0.000000000000     0.2515E-04   0.5187E-07
     2   0.362556247141    0.000000000000     0.1234E-04   0.4486E-07
     3   0.370075778232    0.000000000000     0.1004E-03   0.7606E-05
     4   0.458379551569    0.000000000000     0.3957E-02   0.3875E-03
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229052697    0.000000000000     0.4534E-05   0.7306E-07
     2   0.362556386914    0.000000000000     0.5170E-05   0.1398E-06
     3   0.370074393890    0.000000000000     0.3349E-04   0.1384E-05
     4   0.458395961760    0.000000000000     0.1156E-02   0.1641E-04
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072814    0.000000000000     0.1368E-05   0.2012E-07
     2   0.362556338094    0.000000000000     0.2188E-05   0.4882E-07
     3   0.370073847672    0.000000000000     0.1109E-04   0.5462E-06
     4   0.458389891513    0.000000000000     0.3990E-03   0.6070E-05
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229074371    0.000000000000     0.4462E-06   0.1557E-08
     2   0.362556334559    0.000000000000     0.7471E-06   0.3536E-08
     3   0.370073917470    0.000000000000     0.3265E-05   0.6980E-07
     4   0.458391350190    0.000000000000     0.1317E-03   0.1459E-05
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072224    0.000000000000     0.8175E-07   0.2147E-08
     2   0.362556340021    0.000000000000     0.1599E-06   0.5463E-08
     3   0.370073923626    0.000000000000     0.7712E-06   0.6156E-08
     4   0.458391703930    0.000000000000     0.2707E-04   0.3537E-06
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072064    0.000000000000     0.1150E-07   0.1599E-09
     2   0.362556339090    0.000000000000     0.4114E-07   0.9313E-09
     3   0.370073922998    0.000000000000     0.1933E-06   0.6279E-09
     4   0.458391794377    0.000000000000     0.5507E-05   0.9045E-07
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072040    0.000000000000     0.2619E-08   0.2393E-10
     2   0.362556339035    0.000000000000     0.1081E-07   0.5542E-10
     3   0.370073923269    0.000000000000     0.4614E-07   0.2709E-09
     4   0.458391731832    0.000000000000     0.1054E-05   0.6255E-07
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072035    0.000000000000     0.3812E-09   0.4856E-11
     2   0.362556339082    0.000000000000     0.2262E-08   0.4721E-10
     3   0.370073923301    0.000000000000     0.9507E-08   0.3247E-10
     4   0.458391732410    0.000000000000     0.1978E-06   0.5784E-09
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   68

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.6329E-10   0.3988E-11
     2   0.362556339072    0.000000000000     0.2832E-09   0.9533E-11
     3   0.370073923294    0.000000000000     0.1229E-08   0.6860E-11
     4   0.458391732882    0.000000000000     0.3246E-07   0.4718E-09
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   72

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.7096E-11   0.1453E-12
     2   0.362556339072    0.000000000000     0.6397E-10   0.5110E-12
     3   0.370073923291    0.000000000000     0.2584E-09   0.3624E-11
     4   0.458391732780    0.000000000000     0.6070E-08   0.1017E-09
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   75

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.6818E-11   0.1665E-14
     2   0.362556339072    0.000000000000     0.6639E-11   0.2591E-12
     3   0.370073923291    0.000000000000     0.2639E-10   0.1420E-12
     4   0.458391732792    0.000000000000     0.1213E-08   0.1206E-10
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   77

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.6809E-11   0.5218E-14
     2   0.362556339072    0.000000000000     0.1870E-11   0.6661E-15
     3   0.370073923291    0.000000000000     0.4534E-11   0.5235E-13
     4   0.458391732787    0.000000000000     0.1875E-09   0.5637E-11
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   78

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.6799E-11   0.2665E-14
     2   0.362556339072    0.000000000000     0.1828E-11   0.5107E-14
     3   0.370073923291    0.000000000000     0.4408E-11   0.1776E-14
     4   0.458391732786    0.000000000000     0.2536E-10   0.3466E-12
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   79

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.264229072039    0.000000000000     0.6790E-11   0.3497E-14
     2   0.362556339072    0.000000000000     0.1832E-11   0.3886E-15
     3   0.370073923291    0.000000000000     0.4374E-11   0.2831E-14
     4   0.458391732787    0.000000000000     0.3870E-11   0.1013E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 22 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.264229072039
     Fraction singles (|L1|/|L|):       0.969223834384

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      5        0.934221235411
       16      5       -0.218875252130
       15      5       -0.107640493576
       10      5        0.049754842253
        6      5        0.040398551754
        3      5       -0.035312305115
       13      5        0.026539305106
        8      5       -0.016098006530
       19      5       -0.016087860042
        1      6        0.011236720954
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.362556339072
     Fraction singles (|L1|/|L|):       0.985710163886

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        2      5        0.856072379442
       14      5        0.429332447519
        5      5        0.159444206387
       23      5        0.143027160058
       12      5       -0.078691144172
       18      5        0.043525069274
       22      5        0.012017784241
        2      6        0.009557573871
       14      6        0.007711482762
       17      2        0.006483206494
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.370073923291
     Fraction singles (|L1|/|L|):       0.965785921785

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      4       -0.909356693205
       16      4        0.232685284691
        2      3        0.131545360700
       15      4        0.114469503842
        6      4       -0.079508642210
        1      2        0.067145698026
       10      4       -0.048034293901
       14      3        0.043314789800
       13      4       -0.040671867546
        4      5        0.036061051014
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.458391732787
     Fraction singles (|L1|/|L|):       0.984408370815

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        2      4       -0.683496086340
        1      3        0.536048067473
       14      4       -0.386714255138
        5      4       -0.137146689866
       16      3       -0.129555674189
       23      4       -0.122470084888
       12      4        0.079876929869
       15      3       -0.062147140924
       10      3        0.036730522570
       18      4       -0.035285406476
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.264229072039        7.190039276965
        2                  0.362556339072        9.865660496486
        3                  0.370073923291       10.070224382609
        4                  0.458391732787       12.473474389231
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (left)

     Total wall time (sec):              0.42200
     Total cpu time (sec):               0.62365

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              0.59300
     Total cpu time (sec):               0.95711

  eT terminated successfully!
