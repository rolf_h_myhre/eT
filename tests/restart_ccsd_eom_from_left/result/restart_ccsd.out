


                     eT - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, M. Scavino, A. Skeidsvoll, 
   Å. H. Tveten
  ------------------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        restart
        response
     end do

     memory
        available: 8
     end memory

     cc response
        eom
        dipole length
        transition moments
     end cc response

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     solver cc multipliers
        threshold: 1.0d-12
     end solver cc multipliers

     solver cc es
        singlet states:     1
        residual threshold: 1.0d-11
        energy threshold:   1.0d-11
     end solver cc es


  Running on 2 OMP thread(s)
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.116100574587
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29
  restart variable: True


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Calculation of reference state (SCF-DIIS algorithm)


  1) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Energy threshold:              0.1000E-10
     Gradient threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Requested restart. Reading orbitals from file

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.843851693631     0.2908E-11     0.7884E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  Note: the gradient converged in the first iteration,  so the energy 
  convergence has not been tested!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080248
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268217
     Total energy:                 -78.843851693631

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574269195145   9   0.704416610867  17   1.682695299877  25   3.175416260660
   2  -1.277667044968  10   0.746260122914  18   1.804186820052  26   3.209661156313
   3  -0.898849406810  11   1.155862024383  19   1.902641648742  27   3.328173215200
   4  -0.629870222690  12   1.170770887076  20   2.148883457168  28   3.721936473470
   5  -0.541641772852  13   1.267961746552  21   2.200395756969  29   3.985492632586
   6  -0.485872762832  14   1.449847537220  22   2.540321860021
   7   0.159756317416  15   1.463234913438  23   2.541517310547
   8   0.229311906283  16   1.474444394527  24   2.559338707350
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.04943
     Total cpu time (sec):               0.05238


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Coupled cluster response engine
  =====================================

  Calculates dipole transition moments and oscillator strengths between 

  the ground state and the excited states.

  This is a CCSD response calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the multipliers (davidson algorithm)
     5) Calculation of the excited state (davidson algorithm)
     6) Calculation of the transition moments (EOM)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.19807
     Total cpu time (sec):               0.39406


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Omega threshold:           0.10E-10
     Energy threshold:          0.10E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Requested restart. Reading in solution from file.

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.100383481556     0.7516E-11     0.7910E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  Note: the omega vector converged in the first iteration,  so the energy 
  convergence has not been tested!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100383481556

     Correlation energy (a.u.):           -0.256531787925

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014740597524
       14      4       -0.009546856220
        7      4        0.008284826483
       15      5       -0.006124828873
        4      5        0.005606072698
        6      2        0.005476844297
        2      4        0.005318591701
       13      5        0.005269818337
        5      6        0.004933006906
       11      6       -0.003454309399
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047351708919
        5      6       5      6       -0.046240574404
        9      3       9      3       -0.041367012248
        3      4       3      4       -0.036659067517
        6      5       6      5       -0.034554012170
        1      5       1      5       -0.034177347751
       16      3      16      3       -0.032108235347
       17      3      17      3       -0.032052553603
       18      3      18      3       -0.031351828684
        2      4       3      4       -0.029701270698
     --------------------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007226152112

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.01776
     Total cpu time (sec):               0.03376


  4) Calculation of the multipliers (davidson algorithm)

   - Davidson coupled cluster multipliers solver
  -------------------------------------------------

  A Davidson solver that solves the multiplier equation: t-bar^T A = -η. 
  This linear equation is solved in a reduced space. A description of 
  the algorithm can be found in E. R. Davidson, J. Comput. Phys. 17, 87 
  (1975).

  - Davidson CC multipliers solver settings:

     Residual threshold:        0.10E-11
     Max number of iterations:       100

  Iteration     Residual norm
  ---------------------------
    1             0.3520E-01
    2             0.6901E-02
    3             0.1186E-02
    4             0.6190E-03
    5             0.1749E-03
    6             0.4640E-04
    7             0.1050E-04
    8             0.3165E-05
    9             0.1472E-05
   10             0.6431E-06
   11             0.2184E-06
   12             0.5525E-07
   13             0.1279E-07
   14             0.2938E-08
   15             0.7460E-09
   16             0.2189E-09
   17             0.7578E-10
   18             0.3361E-10
   19             0.1167E-10
   20             0.4058E-11
   21             0.1156E-11
   22             0.2911E-12
  ---------------------------

  Convergence criterion met in 22 iterations!

  - Davidson CC multipliers solver summary:

     Largest single amplitudes:
     -----------------------------------
        a       i         tbar(a,i)
     -----------------------------------
        1      5        0.022790118897
       14      4       -0.015907507091
        7      4        0.014297181947
        4      5        0.011712306720
       15      5       -0.010007216961
        6      2        0.009083547621
       13      5        0.008770531160
        2      4        0.006954460294
        5      6        0.006820250278
        3      4        0.006765159553
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         tbar(ai,bj)
     --------------------------------------------------
        5      6       5      6       -0.092500754790
        2      4       2      4       -0.092085328213
        6      5       5      6        0.089666625590
        2      4       5      6       -0.083792278099
        9      3       9      3       -0.082167759498
        2      4       6      5        0.077164163662
        2      4       1      5       -0.076342716082
        7      4       5      6        0.071424572152
        3      4       3      4       -0.071282985174
        6      5       6      5       -0.068075967747
     --------------------------------------------------

  - Finished solving the CCSD multipliers equations

     Total wall time (sec):               0.12520
     Total cpu time (sec):                0.25319


  5) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Energy threshold:                0.10E-10
     Residual threshold:              0.10E-10

     Number of singlet states:               1
     Max number of iterations:             100

     Max reduced space dimension:          100

     Restarting right vector 1 from file.

  Iteration:                  1
  Reduced space dimension:    1

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.4061E+00   0.9187E-14
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1116E+00   0.3608E-14
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    3

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.3384E-01   0.2398E-13
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1077E-01   0.6883E-14
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:    5

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.3161E-02   0.5551E-15
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.8020E-03   0.2220E-15
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:    7

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.2235E-03   0.1110E-15
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.7811E-04   0.6661E-15
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:    9

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.2548E-04   0.6661E-15
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1421E-04   0.1388E-14
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   11

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1081E-04   0.1221E-14
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.4465E-05   0.4718E-15
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   13

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1208E-05   0.1943E-15
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.3372E-06   0.0000E+00
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   15

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.2459E-06   0.3053E-15
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.2546E-06   0.5829E-15
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   17

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1084E-06   0.8327E-16
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.4636E-07   0.4996E-15
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   19

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1737E-07   0.1943E-15
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.4573E-08   0.0000E+00
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   21

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1279E-08   0.2498E-15
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.6008E-09   0.0000E+00
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   23

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.2457E-09   0.5274E-15
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.6828E-10   0.1665E-15
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   25

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.1902E-10   0.8327E-15
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.5834E-11   0.4441E-15
  ------------------------------------------------------------------------
  Convergence criterion met in 26 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247194255572
     Fraction singles (|R1|/|R|):       0.973397678083

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6        0.964249868485
        4      6        0.121847295824
        6      6       -0.036249387510
       13      6       -0.030061485952
        1      3       -0.011074241439
       22      6        0.008603353606
       19      5       -0.007863834135
        1      5        0.007857920531
       10      6        0.007675079118
        9      6       -0.007427820492
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.100746791318
        1      5       1      6       -0.085612058155
        1      6       5      6       -0.066933008112
        1      2       1      6       -0.063267573162
        1      4       2      6       -0.061512599149
        3      4       1      6       -0.054854109240
        6      5       1      6        0.046260935196
        4      5       1      6       -0.038260256937
        7      4       1      6        0.032958804244
        4      4       2      6       -0.032734134559
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247194255572        6.726498310303
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.12608
     Total cpu time (sec):               0.25006


  5) Calculation of the excited state (davidson algorithm)
     Calculating left vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and  the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue  problem 
  is solved in a reduced space, the dimension of which is  expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in  E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  left

     Energy threshold:                0.10E-10
     Residual threshold:              0.10E-10

     Number of singlet states:               1
     Max number of iterations:             100

     Max reduced space dimension:          100

     Restarting left vector 1 from file.

  Iteration:                  1
  Reduced space dimension:    1

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247194255572    0.000000000000     0.9080E-11   0.1008E-13
  ------------------------------------------------------------------------
  Note: Residual(s) converged in first iteration. Energy convergence therefore 
  not tested in this calculation.
  Convergence criterion met in 1 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247194255572
     Fraction singles (|L1|/|L|):       0.942536368057

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      6        0.934321066335
        4      6        0.114002382604
        6      6       -0.032814400825
       13      6       -0.027947643734
        1      3       -0.010930020792
        1      5        0.007879253273
       22      6        0.007408446952
       10      6        0.006944886702
       19      5       -0.006882560536
        9      6       -0.006676723237
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         L(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.171079645375
        6      5       1      6        0.100212340581
        3      4       1      6       -0.099658455493
        1      5       1      6       -0.089486547886
        4      5       1      6       -0.079266906024
        1      6       5      6       -0.071510175858
        7      4       1      6        0.068318341917
        1      2       1      6       -0.065029437021
        4      4       2      6       -0.055727685952
        3      5       2      6       -0.042559408816
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247194255572        6.726498310302
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (left)

     Total wall time (sec):              0.00663
     Total cpu time (sec):               0.01463


  6) Calculation of the transition moments (EOM)

  :: EOM properties calculation

  - Summary of EOM properties calculation:

     State 1:
     ----------
     Calculation type:                             EOM
     Excitation energy [E_h]:           0.247194255572
     Excitation energy [eV]:            6.726498310303
     Hartree-to-eV (CODATA 2014):          27.21138602

                   Transition moments [a.u.]         Transition strength [a.u.]
     --------------------------------------------------------------------------
     Comp. q     < k |q| 0 >       < 0 |q| k >        < 0 |q| k > < k |q| 0 >
     --------------------------------------------------------------------------
     X           0.0004665158      0.0008903044            0.0000004153
     Y           0.0027022345      0.0052703759            0.0000142418
     Z          -0.2024326697     -0.3970439121            0.0803746591
     --------------------------------------------------------------------------
     Oscillator strength:      0.013247851457

  - Timings for the CCSD response calculation

     Total wall time (sec):              0.47522
     Total cpu time (sec):               0.95119

     Peak memory usage during the execution of eT: 16.307080 MB

  eT terminated successfully!
