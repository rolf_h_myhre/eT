


                     eT 1.1 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT version 1.1.0, Nachspiel
  ------------------------------------------------------------
  Compiled by:        alexancp
  Compiled on:        NTNU16748
  Configuration date: 2020-11-27 09:33:39 UTC +01:00
  Git branch:         speedup-of-tests
  Git hash:           8c21e529e365b13ed1e9193264a7eb1a530e7451
  Fortran compiler:   GNU 9.3.0
  C compiler:         GNU 9.3.0
  C++ compiler:       GNU 9.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2020-11-27 09:52:42 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        response
     end do

     method
        hf
        ccsd
     end method

     cc response
        eom
        dipole length
        transition moments
     end cc response

     solver scf
        restart
        gradient threshold: 1.0d-11
     end solver scf

     solver cholesky
        threshold: 1.0d-11
     end solver cholesky

     solver cc gs
        restart
        omega threshold: 1.0d-11
     end solver cc gs

     solver cc multipliers
        threshold: 1.0d-11
     end solver cc multipliers

     solver cc es
        restart
        residual threshold: 1.0d-11
        singlet states: 1
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: Molecular system specifications
  =====================================

     Name:             h2o he
     Charge:           0
     Multiplicity:     1
     Coordinate units: angstrom

     Pure basis functions:         29
     Cartesian basis functions:    30
     Primitive basis functions:    56

     Nuclear repulsion energy (a.u.):             12.163673938821
     Bohr/angstrom value (CODATA 2010):            0.529177210920

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      0.866810000000     0.601440000000     5.000000000000        1
        2 H     -0.866810000000     0.601440000000     5.000000000000        2
        3 O      0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000     0.100000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1 H      1.638033502034     1.136556880358     9.448630622825        1
        2 H     -1.638033502034     1.136556880358     9.448630622825        2
        3 O      0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457     0.188972612457    14.172945934238        4
     ==============================================================================


  :: RHF wavefunction
  ======================

  - Cholesky decomposition of AO overlap to get linearly independent orbitals:

  - Orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29
     Number of atomic orbitals:          29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Calculation of reference state (SCF-DIIS algorithm)


  1) Calculation of reference state (SCF-DIIS algorithm)

   - Self-consistent field DIIS Hartree-Fock solver
  ----------------------------------------------------

  A DIIS-accelerated Roothan-Hall self-consistent field solver. A least-square 
  DIIS fit is performed on the previous Fock matrices and associated gradients. 
  Following the Roothan-Hall update of the density, the DIIS-fitted Fock 
  matrix is used to get the next orbital coefficients.

  - Hartree-Fock solver settings:

     DIIS dimension:                         8
     Cumulative Fock threshold:       0.10E+01

     Residual threshold:            0.1000E-10

     Coulomb screening threshold:   0.1000E-16
     Exchange screening threshold:  0.1000E-14
     Fock precision:                0.1000E-33
     Integral cutoff:               0.1000E-16

  - Requested restart. Reading orbitals from file

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.843676337127     0.3045E-11     0.7884E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645839120081
     Nuclear repulsion energy:      12.163673938821
     Electronic energy:            -91.007350275947
     Total energy:                 -78.843676337127

  - Molecular orbital energies

  -----------------------------------------------------------------------------------
   1 -20.574161720883   9   0.704387759569  17   1.680115020157  25   3.175802280618
   2  -1.277649761715  10   0.746901100079  18   1.804193350624  26   3.209692713503
   3  -0.899902596944  11   1.154728902868  19   1.904213902493  27   3.328283990654
   4  -0.629830825440  12   1.170784555105  20   2.148837823370  28   3.722330385569
   5  -0.541608797265  13   1.267987483216  21   2.200396855907  29   3.985536398832
   6  -0.485759965504  14   1.449812798732  22   2.539474155966
   7   0.160079154577  15   1.465208505809  23   2.540985376002
   8   0.229286143882  16   1.477689924381  24   2.561261268944
  -----------------------------------------------------------------------------------

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.07222
     Total cpu time (sec):               0.09448


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False
     Core-valence separation: False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Coupled cluster response engine
  =====================================

  Calculates dipole transition moments and oscillator strengths between 

  the ground state and the excited states.

  This is a CCSD response calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the multipliers (davidson algorithm)
     5) Calculation of the excited state (davidson algorithm)
     6) Calculation of the transition moments (EOM)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-10
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition system details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               405 /     107       0.47383E+01         147             42             17010
     2               318 /      92       0.47167E-01         234            111             35298
     3               247 /      75       0.46940E-03         178            183             45201
     4               181 /      53       0.38429E-05         146            267             48327
     5                70 /      18       0.38181E-07          73            325             22750
     6                 0 /       0       0.37014E-09          36            345                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 345

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.8367E-11
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.17517
     Total cpu time (sec):               0.35117


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - DIIS CC ground state solver settings:

     Residual threshold:            0.1000E-10

     DIIS dimension:                   8
     Max number of iterations:       100

     Storage: disk

  Requested restart. Reading in solution from file.

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.100220193412     0.7823E-11     0.7910E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.100220193412

     Correlation energy (a.u.):           -0.256543856285

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.014747369533
       14      4       -0.009547136381
        7      4        0.008286082476
       15      5       -0.006127815615
        4      5        0.005597168702
        6      2        0.005482270194
        2      4        0.005315676356
       13      5        0.005262264632
        5      6       -0.004976931950
       11      6       -0.003468458373
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.047348347761
        5      6       5      6       -0.046125315414
        3      4       3      4       -0.036665602909
        6      5       6      5       -0.034567429546
        1      5       1      5       -0.034196788115
       16      3      16      3       -0.032093077498
       17      3      17      3       -0.032023970211
       18      3      18      3       -0.031207949792
        9      3       9      3       -0.030955102756
        2      4       3      4       -0.029700794879
     --------------------------------------------------

     T1 diagnostic (|T1|/sqrt(N_e)): 0.007230547545

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.01649
     Total cpu time (sec):               0.03249


  4) Calculation of the multipliers (davidson algorithm)

   - Davidson coupled cluster multipliers solver
  -------------------------------------------------

  A Davidson solver that solves the multiplier equation: t-bar^T A = -η. 
  This linear equation is solved in a reduced space. A description of 
  the algorithm can be found in E. R. Davidson, J. Comput. Phys. 17, 87 
  (1975).

  - Davidson CC multipliers solver settings:

     Residual threshold:        0.10E-10
     Max number of iterations:       100

  Iteration     Residual norm
  ---------------------------
    1             0.3517E-01
    2             0.6903E-02
    3             0.1186E-02
    4             0.6184E-03
    5             0.1750E-03
    6             0.4651E-04
    7             0.1074E-04
    8             0.3420E-05
    9             0.1679E-05
   10             0.6733E-06
   11             0.2193E-06
   12             0.5501E-07
   13             0.1272E-07
   14             0.2939E-08
   15             0.7566E-09
   16             0.2298E-09
   17             0.8312E-10
   18             0.3665E-10
   19             0.1210E-10
   20             0.4111E-11
  ---------------------------

  Convergence criterion met in 20 iterations!

  - Davidson CC multipliers solver summary:

     Largest single amplitudes:
     -----------------------------------
        a       i         tbar(a,i)
     -----------------------------------
        1      5        0.022805147344
       14      4       -0.015908633383
        7      4        0.014301022446
        4      5        0.011694825280
       15      5       -0.010012933936
        6      2        0.009090546441
       13      5        0.008759484934
        2      4        0.006944367808
        5      6       -0.006882875140
        3      4        0.006757635369
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         tbar(ai,bj)
     --------------------------------------------------
        5      6       5      6       -0.092268813941
        2      4       2      4       -0.092077851559
        6      5       5      6       -0.089587402693
        2      4       5      6        0.083766793134
        2      4       6      5        0.077165954299
        2      4       1      5       -0.076349534109
        7      4       5      6       -0.071310533230
        3      4       3      4       -0.071295647619
        6      5       6      5       -0.068100598733
        1      5       1      5       -0.065946545915
     --------------------------------------------------

  - Finished solving the CCSD multipliers equations

     Total wall time (sec):               0.08824
     Total cpu time (sec):                0.17623


  5) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

     Residual threshold:            0.1000E-10

     Number of singlet states:               1
     Max number of iterations:             100

     Max reduced space dimension:          100

     Restarting right vector 1 from file r_001.

  Iteration:                  1
  Reduced space dimension:    1

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982155    0.000000000000     0.7320E-11   0.2465E-13
  ------------------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247312982155
     Fraction singles (|R1|/|R|):       0.973375794335

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6        0.964216902506
        4      6        0.121770726882
        6      6       -0.036132124926
       13      6       -0.029902629305
        1      3       -0.011600023306
       10      6        0.010013571740
        1      5        0.008692885363
       22      6        0.008618559502
       19      5       -0.007874999592
       20      4       -0.007260549449
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.100761726289
        1      5       1      6       -0.085630628248
        1      6       5      6        0.066910787427
        1      2       1      6       -0.063308996815
        1      4       2      6       -0.061583402730
        3      4       1      6       -0.054907735002
        6      5       1      6        0.046252870581
        4      5       1      6       -0.038223994234
        7      4       1      6        0.032960704763
        4      4       2      6       -0.032722188241
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247312982155        6.729729025172
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.00556
     Total cpu time (sec):               0.00956


  5) Calculation of the excited state (davidson algorithm)
     Calculating left vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  left

     Residual threshold:            0.1000E-10

     Number of singlet states:               1
     Max number of iterations:             100

     Max reduced space dimension:          100

     Restarting left vector 1 from file r_001.

  Iteration:                  1
  Reduced space dimension:    1

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.234044636033    0.000000000000     0.1288E+00   0.1327E-01
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247304896199    0.000000000000     0.2737E-01   0.1326E-01
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    3

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247191844407    0.000000000000     0.8787E-02   0.1131E-03
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247314800884    0.000000000000     0.2077E-02   0.1230E-03
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:    5

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247313748698    0.000000000000     0.6258E-03   0.1052E-05
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247311875615    0.000000000000     0.1612E-03   0.1873E-05
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:    7

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312747790    0.000000000000     0.5020E-04   0.8722E-06
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247313042429    0.000000000000     0.2155E-04   0.2946E-06
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:    9

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247313072493    0.000000000000     0.2073E-04   0.3006E-07
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312966377    0.000000000000     0.8077E-05   0.1061E-06
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   11

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312977882    0.000000000000     0.2437E-05   0.1151E-07
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312983617    0.000000000000     0.5150E-06   0.5734E-08
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   13

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312981880    0.000000000000     0.1587E-06   0.1736E-08
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312981816    0.000000000000     0.5490E-07   0.6486E-10
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   15

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982160    0.000000000000     0.2797E-07   0.3449E-09
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982295    0.000000000000     0.2055E-07   0.1350E-09
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   17

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982163    0.000000000000     0.1747E-07   0.1325E-09
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982081    0.000000000000     0.9217E-08   0.8192E-10
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   19

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982129    0.000000000000     0.3553E-08   0.4839E-10
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982161    0.000000000000     0.1963E-08   0.3156E-10
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   21

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982161    0.000000000000     0.8717E-09   0.3268E-12
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982156    0.000000000000     0.3425E-09   0.4893E-11
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   23

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982155    0.000000000000     0.1256E-09   0.5170E-12
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982155    0.000000000000     0.4613E-10   0.5245E-12
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   25

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982155    0.000000000000     0.1904E-10   0.1510E-12
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.247312982155    0.000000000000     0.7036E-11   0.1309E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 26 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.247312982155
     Fraction singles (|L1|/|L|):       0.942531032639

     Largest single amplitudes:
     -----------------------------------
        a       i         L(a,i)
     -----------------------------------
        1      6        0.934302980285
        4      6        0.113940647873
        6      6       -0.032704845192
       13      6       -0.027802552761
        1      3       -0.011444229191
       10      6        0.009054018780
        1      5        0.008730825108
       22      6        0.007422819385
       19      5       -0.006892929045
        8      4       -0.006439007669
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         L(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.171035162951
        6      5       1      6        0.100231515517
        3      4       1      6       -0.099701690103
        1      5       1      6       -0.089497856571
        4      5       1      6       -0.079150704119
        1      6       5      6        0.071472425437
        7      4       1      6        0.068295546904
        1      2       1      6       -0.065072321962
        4      4       2      6       -0.055704061628
        3      5       2      6       -0.042583103145
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.247312982155        6.729729025170
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (left)

     Total wall time (sec):              0.12754
     Total cpu time (sec):               0.25162


  6) Calculation of the transition moments (EOM)

  :: EOM properties calculation

  - Summary of EOM properties calculation:

     State 1:
     ----------
     Calculation type:                             EOM
     Excitation energy [E_h]:           0.247312982155
     Excitation energy [eV]:            6.729729025172
     Hartree-to-eV (CODATA 2014):          27.21138602

                   Transition moments [a.u.]         Transition strength [a.u.]
     --------------------------------------------------------------------------
     Comp. q     < k |q| 0 >       < 0 |q| k >        < 0 |q| k > < k |q| 0 >
     --------------------------------------------------------------------------
     X           0.0005582095      0.0010645411            0.0000005942
     Y           0.0028939558      0.0056493936            0.0000163491
     Z          -0.2019462617     -0.3960659967            0.0799840474
     --------------------------------------------------------------------------
     Oscillator strength:      0.013190189064

  - Timings for the CCSD response calculation

     Total wall time (sec):              0.41438
     Total cpu time (sec):               0.82644

     Peak memory usage during the execution of eT: 16.013152 MB

  Calculation ended: 2020-11-27 09:52:42 UTC +01:00

  eT terminated successfully!
